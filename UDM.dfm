object DM: TDM
  OldCreateOrder = False
  Height = 423
  Width = 732
  object Ds_Referencia: TDataSource
    AutoEdit = False
    DataSet = Q_Referencia
    Left = 125
    Top = 24
  end
  object Ds_Cons_Referencia: TDataSource
    DataSet = Q_Cons_Referencia
    Left = 197
    Top = 112
  end
  object Q_Cons_Referencia_Nivel: TOracleDataSet
    SQL.Strings = (
      'SELECT C.CONTEUDO, C.DESCRICAO, C.ID_NIVEL_PRODUTO '
      '  FROM REFERENCIA_NIVEL V, NIVEL_CONTEUDO C'
      ' WHERE V.ID_NIVEL_CONTEUDO = C.ID_NIVEL_CONTEUDO'
      '   AND V.ID_REFERENCIA = :p_id_referencia'
      '   AND C.ID_NIVEL_PRODUTO = :p_id_nivel_produto ')
    Optimize = False
    Variables.Data = {
      0400000002000000200000003A0050005F00490044005F005200450046004500
      520045004E00430049004100050000000000000000000000260000003A005000
      5F00490044005F004E004900560045004C005F00500052004F00440055005400
      4F00030000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      05000000030000001000000043004F004E0054004500550044004F0001000000
      000012000000440045005300430052004900430041004F000100000000002000
      0000490044005F004E004900560045004C005F00500052004F00440055005400
      4F00010000000000}
    Session = orabanco
    Left = 500
    Top = 8
    object Q_Cons_Referencia_NivelCONTEUDO: TStringField
      FieldName = 'CONTEUDO'
      Required = True
      Size = 10
    end
    object Q_Cons_Referencia_NivelDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object Q_Cons_Referencia_NivelID_NIVEL_PRODUTO: TFloatField
      FieldName = 'ID_NIVEL_PRODUTO'
      Required = True
    end
  end
  object Q_Referencia_Nivel: TOracleDataSet
    SQL.Strings = (
      'select n.rowid, '
      '       n.id_referencia_nivel, '
      '       n.id_referencia, '
      '       n.id_nivel_conteudo '
      '  from referencia_nivel n'
      ' where n.id_referencia = :p_id_referencia')
    Optimize = False
    Variables.Data = {
      0400000001000000200000003A0050005F00490044005F005200450046004500
      520045004E00430049004100050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000000300000026000000490044005F005200450046004500520045004E00
      4300490041005F004E004900560045004C000100000000001A00000049004400
      5F005200450046004500520045004E0043004900410001000000000022000000
      490044005F004E004900560045004C005F0043004F004E005400450055004400
      4F00010000000000}
    LockingMode = lmNone
    CommitOnPost = False
    Session = orabanco
    Left = 517
    Top = 64
    object Q_Referencia_NivelID_REFERENCIA_NIVEL: TFloatField
      FieldName = 'ID_REFERENCIA_NIVEL'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object Q_Referencia_NivelID_REFERENCIA: TStringField
      FieldName = 'ID_REFERENCIA'
      Required = True
      Size = 50
    end
    object Q_Referencia_NivelID_NIVEL_CONTEUDO: TFloatField
      FieldName = 'ID_NIVEL_CONTEUDO'
      Required = True
    end
  end
  object Q_Referencia: TOracleDataSet
    SQL.Strings = (
      'select   r.rowid,'
      #9' r.id_referencia,'
      '         r.descricao,'
      '         r.descricao_etiqueta,'
      '         r.foto1,'
      '         r.foto2,'
      '         r.foto3,'
      '         r.id_forma,'
      '         r.numero_base,'
      '         r.id_grade, '
      '         r.id_cor,'
      '         r.id_tipo_material,'
      '         r.id_tipo_solado,'
      '         r.id_tipo_corte,'
      '         r.id_colecao,'
      '         r.id_evento,'
      '         r.id_linha_produto,'
      '         r.id_designer,'
      '         r.id_modelista,'
      '         r.id_cliente,'
      '         r.observacao,'
      '         r.vigencia,'
      '         r.id_tipo_produto,'
      '         r.descricao_faturamento,'
      '         r.id_unidade,'
      '         r.id_ncm,'
      '         r.peso_bruto,'
      '         r.peso_liquido'
      '    from pm.referencia r'
      '   where r.id_referencia = :p_referencia')
    Optimize = False
    Variables.Data = {
      04000000010000001A0000003A0050005F005200450046004500520045004E00
      430049004100050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000001B00000012000000440045005300430052004900430041004F000100
      0000000024000000440045005300430052004900430041004F005F0045005400
      4900510055004500540041000100000000000A00000046004F0054004F003100
      0000000000000A00000046004F0054004F0032000000000000000A0000004600
      4F0054004F003300000000000000160000004E0055004D00450052004F005F00
      4200410053004500010000000000140000004F00420053004500520056004100
      430041004F000100000000001A000000490044005F0052004500460045005200
      45004E004300490041000100000000000C000000490044005F0043004F005200
      01000000000020000000490044005F005400490050004F005F004D0041005400
      45005200490041004C000100000000001C000000490044005F00540049005000
      4F005F0053004F004C00410044004F000100000000001A000000490044005F00
      5400490050004F005F0043004F00520054004500010000000000140000004900
      44005F0043004F004C004500430041004F000100000000001200000049004400
      5F004500560045004E0054004F0001000000000020000000490044005F004C00
      49004E00480041005F00500052004F004400550054004F000100000000001600
      0000490044005F00440045005300490047004E00450052000100000000001800
      0000490044005F004D004F00440045004C004900530054004100010000000000
      1000000056004900470045004E004300490041000100000000001E0000004900
      44005F005400490050004F005F00500052004F004400550054004F0001000000
      000010000000490044005F0046004F0052004D00410001000000000014000000
      490044005F0043004C00490045004E0054004500010000000000100000004900
      44005F00470052004100440045000100000000002A0000004400450053004300
      52004900430041004F005F004600410054005500520041004D0045004E005400
      4F0001000000000014000000490044005F0055004E0049004400410044004500
      0100000000000C000000490044005F004E0043004D0001000000000014000000
      5000450053004F005F0042005200550054004F00010000000000180000005000
      450053004F005F004C00490051005500490044004F00010000000000}
    LockingMode = lmNone
    CommitOnPost = False
    Session = orabanco
    Left = 349
    Top = 104
    object Q_ReferenciaCliente_Razao_Social: TStringField
      FieldKind = fkCalculated
      FieldName = 'Cliente_Razao_Social'
      Size = 40
      Calculated = True
    end
    object Q_ReferenciaForma_Descricao: TStringField
      DisplayLabel = 'Descri'#231#227'o da Forma'
      FieldKind = fkCalculated
      FieldName = 'Forma_Descricao'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaTipo_Material_Descricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'Tipo_Material_Descricao'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaTipo_Solado_Descricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'Tipo_Solado_Descricao'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaTipo_Corte_Descricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'Tipo_Corte_Descricao'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaID_REFERENCIA: TStringField
      FieldName = 'ID_REFERENCIA'
      Required = True
      Size = 50
    end
    object Q_ReferenciaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object Q_ReferenciaDESCRICAO_ETIQUETA: TStringField
      FieldName = 'DESCRICAO_ETIQUETA'
      Required = True
      Size = 15
    end
    object Q_ReferenciaFOTO1: TBlobField
      FieldName = 'FOTO1'
      BlobType = ftOraBlob
    end
    object Q_ReferenciaFOTO2: TBlobField
      FieldName = 'FOTO2'
      BlobType = ftOraBlob
    end
    object Q_ReferenciaFOTO3: TBlobField
      FieldName = 'FOTO3'
      BlobType = ftOraBlob
    end
    object Q_ReferenciaNUMERO_BASE: TStringField
      FieldName = 'NUMERO_BASE'
      Required = True
      Size = 5
    end
    object Q_ReferenciaID_COR: TStringField
      FieldName = 'ID_COR'
      Required = True
      Size = 30
    end
    object Q_ReferenciaID_TIPO_MATERIAL: TFloatField
      FieldName = 'ID_TIPO_MATERIAL'
      Required = True
    end
    object Q_ReferenciaID_TIPO_SOLADO: TFloatField
      FieldName = 'ID_TIPO_SOLADO'
      Required = True
    end
    object Q_ReferenciaID_TIPO_CORTE: TFloatField
      FieldName = 'ID_TIPO_CORTE'
      Required = True
    end
    object Q_ReferenciaDescricao_Evento: TStringField
      FieldKind = fkCalculated
      FieldName = 'Descricao_Evento'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaID_COLECAO: TFloatField
      FieldName = 'ID_COLECAO'
    end
    object Q_ReferenciaID_EVENTO: TFloatField
      FieldName = 'ID_EVENTO'
    end
    object Q_ReferenciaID_LINHA_PRODUTO: TFloatField
      FieldName = 'ID_LINHA_PRODUTO'
    end
    object Q_ReferenciaNome_Designer: TStringField
      FieldKind = fkCalculated
      FieldName = 'Nome_Designer'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaNome_Modelista: TStringField
      FieldKind = fkCalculated
      FieldName = 'Nome_Modelista'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaID_DESIGNER: TFloatField
      FieldName = 'ID_DESIGNER'
    end
    object Q_ReferenciaID_MODELISTA: TFloatField
      FieldName = 'ID_MODELISTA'
    end
    object Q_ReferenciaOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 4000
    end
    object Q_ReferenciaVIGENCIA: TDateTimeField
      FieldName = 'VIGENCIA'
    end
    object Q_ReferenciaID_TIPO_PRODUTO: TFloatField
      FieldName = 'ID_TIPO_PRODUTO'
    end
    object Q_ReferenciaDescricao_Colecao: TStringField
      FieldKind = fkCalculated
      FieldName = 'Descricao_Colecao'
      Size = 30
      Calculated = True
    end
    object Q_ReferenciaTipo_Produto_Descricao: TStringField
      DisplayLabel = 'Descri'#231#227'o do tipo de Produto'
      FieldKind = fkCalculated
      FieldName = 'Tipo_Produto_Descricao'
      Calculated = True
    end
    object Q_ReferenciaID_FORMA: TStringField
      FieldName = 'ID_FORMA'
      Required = True
      Size = 15
    end
    object Q_ReferenciaID_CLIENTE: TFloatField
      FieldName = 'ID_CLIENTE'
    end
    object Q_ReferenciaID_GRADE: TFloatField
      FieldName = 'ID_GRADE'
    end
    object Q_ReferenciaDESCRICAO_FATURAMENTO: TStringField
      FieldName = 'DESCRICAO_FATURAMENTO'
      Size = 100
    end
    object Q_ReferenciaID_UNIDADE: TStringField
      FieldName = 'ID_UNIDADE'
      Size = 4
    end
    object Q_ReferenciaID_NCM: TStringField
      FieldName = 'ID_NCM'
      Size = 8
    end
    object Q_ReferenciaPESO_BRUTO: TFloatField
      FieldName = 'PESO_BRUTO'
    end
    object Q_ReferenciaPESO_LIQUIDO: TFloatField
      FieldName = 'PESO_LIQUIDO'
    end
  end
  object Q_Integra_Ref_Produto: TOracleDataSet
    SQL.Strings = (
      'select r.rowid,'
      '       r.id_produto,'
      '       r.codigo,'
      '       r.descricao,'
      '       r.id_unidade_producao,'
      '       r.id_unidade_venda,'
      '       r.id_sub_grupo,'
      '       r.id_grade,'
      '       r.id_produto_relacionado,'
      '       r.fator_conver_compra_producao,'
      '       r.fator_conver_producao_venda,'
      '       r.perc_aceite_entrada_compra,'
      '       r.perc_aceite_pedido_compra,'
      '       r.perc_aceite_saida_consumo,'
      '       r.instrucao_recebimento,'
      '       r.ativo,'
      '       r.ean14'
      '  from produto r'
      ' where r.codigo = :p_codigo')
    Optimize = False
    Variables.Data = {
      0400000001000000120000003A0050005F0043004F004400490047004F000500
      00000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000001000000014000000490044005F00500052004F004400550054004F00
      0100000000000C00000043004F004400490047004F0001000000000012000000
      440045005300430052004900430041004F000100000000002600000049004400
      5F0055004E00490044004100440045005F00500052004F004400550043004100
      4F0001000000000020000000490044005F0055004E0049004400410044004500
      5F00560045004E004400410001000000000018000000490044005F0053005500
      42005F0047005200550050004F0001000000000010000000490044005F004700
      52004100440045000100000000002C000000490044005F00500052004F004400
      550054004F005F00520045004C004100430049004F004E00410044004F000100
      00000000380000004600410054004F0052005F0043004F004E00560045005200
      5F0043004F004D005000520041005F00500052004F0044005500430041004F00
      010000000000360000004600410054004F0052005F0043004F004E0056004500
      52005F00500052004F0044005500430041004F005F00560045004E0044004100
      0100000000003400000050004500520043005F00410043004500490054004500
      5F0045004E00540052004100440041005F0043004F004D005000520041000100
      000000003200000050004500520043005F004100430045004900540045005F00
      500045004400490044004F005F0043004F004D00500052004100010000000000
      3200000050004500520043005F004100430045004900540045005F0053004100
      4900440041005F0043004F004E00530055004D004F000100000000002A000000
      49004E005300540052005500430041004F005F00520045004300450042004900
      4D0045004E0054004F000100000000000A00000041005400490056004F000100
      000000000A000000450041004E0031003400010000000000}
    LockingMode = lmNone
    CommitOnPost = False
    Session = orabanco
    Left = 232
    Top = 32
    object Q_Integra_Ref_ProdutoID_PRODUTO: TFloatField
      FieldName = 'ID_PRODUTO'
      Required = True
    end
    object Q_Integra_Ref_ProdutoCODIGO: TStringField
      FieldName = 'CODIGO'
      Required = True
      Size = 50
    end
    object Q_Integra_Ref_ProdutoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object Q_Integra_Ref_ProdutoID_UNIDADE_PRODUCAO: TStringField
      FieldName = 'ID_UNIDADE_PRODUCAO'
      Required = True
      Size = 4
    end
    object Q_Integra_Ref_ProdutoID_UNIDADE_VENDA: TStringField
      FieldName = 'ID_UNIDADE_VENDA'
      Required = True
      Size = 4
    end
    object Q_Integra_Ref_ProdutoID_SUB_GRUPO: TFloatField
      FieldName = 'ID_SUB_GRUPO'
      Required = True
    end
    object Q_Integra_Ref_ProdutoID_GRADE: TFloatField
      FieldName = 'ID_GRADE'
    end
    object Q_Integra_Ref_ProdutoID_PRODUTO_RELACIONADO: TFloatField
      FieldName = 'ID_PRODUTO_RELACIONADO'
    end
    object Q_Integra_Ref_ProdutoFATOR_CONVER_COMPRA_PRODUCAO: TFloatField
      FieldName = 'FATOR_CONVER_COMPRA_PRODUCAO'
    end
    object Q_Integra_Ref_ProdutoFATOR_CONVER_PRODUCAO_VENDA: TFloatField
      FieldName = 'FATOR_CONVER_PRODUCAO_VENDA'
    end
    object Q_Integra_Ref_ProdutoPERC_ACEITE_ENTRADA_COMPRA: TFloatField
      FieldName = 'PERC_ACEITE_ENTRADA_COMPRA'
    end
    object Q_Integra_Ref_ProdutoPERC_ACEITE_PEDIDO_COMPRA: TFloatField
      FieldName = 'PERC_ACEITE_PEDIDO_COMPRA'
    end
    object Q_Integra_Ref_ProdutoPERC_ACEITE_SAIDA_CONSUMO: TFloatField
      FieldName = 'PERC_ACEITE_SAIDA_CONSUMO'
    end
    object Q_Integra_Ref_ProdutoINSTRUCAO_RECEBIMENTO: TStringField
      FieldName = 'INSTRUCAO_RECEBIMENTO'
      Size = 200
    end
    object Q_Integra_Ref_ProdutoATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
    object Q_Integra_Ref_ProdutoEAN14: TStringField
      FieldName = 'EAN14'
      Size = 14
    end
  end
  object Q_Integra_Ref_Produto_Numero: TOracleDataSet
    SQL.Strings = (
      'select n.rowid,'
      '       n.id_produto_numero, '
      '       n.id_produto, '
      '       n.posicao, '
      '       n.numero, '
      '       n.ean13, '
      '       n.ativo'
      '  from produto_numero n'
      ' where n.id_produto_numero = :p_id_produto_numero')
    Optimize = False
    Variables.Data = {
      0400000001000000280000003A0050005F00490044005F00500052004F004400
      550054004F005F004E0055004D00450052004F00030000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000000600000014000000490044005F00500052004F004400550054004F00
      0100000000000A00000041005400490056004F00010000000000220000004900
      44005F00500052004F004400550054004F005F004E0055004D00450052004F00
      0100000000000E00000050004F0053004900430041004F000100000000000C00
      00004E0055004D00450052004F000100000000000A000000450041004E003100
      3300010000000000}
    LockingMode = lmNone
    CommitOnPost = False
    Session = orabanco
    Left = 384
    Top = 24
    object Q_Integra_Ref_Produto_NumeroID_PRODUTO_NUMERO: TFloatField
      FieldName = 'ID_PRODUTO_NUMERO'
    end
    object Q_Integra_Ref_Produto_NumeroID_PRODUTO: TFloatField
      FieldName = 'ID_PRODUTO'
      Required = True
    end
    object Q_Integra_Ref_Produto_NumeroPOSICAO: TIntegerField
      FieldName = 'POSICAO'
      Required = True
    end
    object Q_Integra_Ref_Produto_NumeroNUMERO: TStringField
      FieldName = 'NUMERO'
      Required = True
      Size = 10
    end
    object Q_Integra_Ref_Produto_NumeroEAN13: TStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object Q_Integra_Ref_Produto_NumeroATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
  end
  object Q_Referencia_Produto_Numero: TOracleDataSet
    SQL.Strings = (
      'select p.rowid,'
      '       p.id_produto_numero, '
      '       p.id_produto, '
      '       p.posicao, '
      '       p.numero, '
      '       p.ean13, '
      '       p.ativo   '
      ' from produto_numero p,'
      '      produto pp'
      '  where p.id_produto = pp.id_produto'
      '    and pp.codigo = :p_codigo_produto'
      '   order by p.posicao')
    Optimize = False
    Variables.Data = {
      0400000001000000220000003A0050005F0043004F004400490047004F005F00
      500052004F004400550054004F00050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000000600000022000000490044005F00500052004F004400550054004F00
      5F004E0055004D00450052004F0001000000000014000000490044005F005000
      52004F004400550054004F000100000000000E00000050004F00530049004300
      41004F000100000000000C0000004E0055004D00450052004F00010000000000
      0A000000450041004E00310033000100000000000A0000004100540049005600
      4F00010000000000}
    Session = orabanco
    Left = 640
    Top = 232
    object Q_Referencia_Produto_NumeroID_PRODUTO_NUMERO: TFloatField
      FieldName = 'ID_PRODUTO_NUMERO'
      Required = True
    end
    object Q_Referencia_Produto_NumeroID_PRODUTO: TFloatField
      FieldName = 'ID_PRODUTO'
      Required = True
    end
    object Q_Referencia_Produto_NumeroPOSICAO: TIntegerField
      FieldName = 'POSICAO'
      Required = True
    end
    object Q_Referencia_Produto_NumeroNUMERO: TStringField
      FieldName = 'NUMERO'
      Required = True
      Size = 10
    end
    object Q_Referencia_Produto_NumeroEAN13: TStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object Q_Referencia_Produto_NumeroATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
  end
  object Q_Cons_Referencia: TOracleDataSet
    SQL.Strings = (
      
        'select r.rowid, r.id_referencia, r.descricao, r.descricao_etique' +
        'ta, '
      '       r.id_forma, r.foto1, r.foto2, r.foto3, r.id_cor'
      '  from pm.referencia r'
      ' where r.id_referencia like :p_id_referencia')
    Optimize = False
    Variables.Data = {
      0400000001000000200000003A0050005F00490044005F005200450046004500
      520045004E00430049004100050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000000800000012000000440045005300430052004900430041004F000100
      0000000024000000440045005300430052004900430041004F005F0045005400
      4900510055004500540041000100000000000A00000046004F0054004F003100
      0000000000000A00000046004F0054004F0032000000000000000A0000004600
      4F0054004F0033000000000000001A000000490044005F005200450046004500
      520045004E004300490041000100000000000C000000490044005F0043004F00
      520001000000000010000000490044005F0046004F0052004D00410001000000
      0000}
    Session = orabanco
    Left = 469
    Top = 112
    object Q_Cons_ReferenciaID_REFERENCIA: TStringField
      DisplayLabel = 'Refer'#234'ncia'
      FieldName = 'ID_REFERENCIA'
      Required = True
      Size = 50
    end
    object Q_Cons_ReferenciaDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      DisplayWidth = 200
      FieldName = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object Q_Cons_ReferenciaDESCRICAO_ETIQUETA: TStringField
      DisplayLabel = 'Desc. Etiqueta'
      FieldName = 'DESCRICAO_ETIQUETA'
      Required = True
      Size = 15
    end
    object Q_Cons_ReferenciaFOTO1: TBlobField
      DisplayLabel = 'Foto1'
      FieldName = 'FOTO1'
      BlobType = ftOraBlob
    end
    object Q_Cons_ReferenciaFOTO2: TBlobField
      DisplayLabel = 'Foto2'
      FieldName = 'FOTO2'
      BlobType = ftOraBlob
    end
    object Q_Cons_ReferenciaFOTO3: TBlobField
      DisplayLabel = 'Foto3'
      FieldName = 'FOTO3'
      BlobType = ftOraBlob
    end
    object Q_Cons_ReferenciaID_COR: TStringField
      DisplayLabel = 'Cor'
      FieldName = 'ID_COR'
      Required = True
      Size = 30
    end
    object Q_Cons_ReferenciaID_FORMA: TStringField
      FieldName = 'ID_FORMA'
      Required = True
      Size = 15
    end
  end
  object Q_Busca_Nivel: TOracleDataSet
    SQL.Strings = (
      'SELECT T.ID_NIVEL_PRODUTO,'
      '       P.DESCRICAO,'
      '       T.SEQUENCIA'
      '  FROM NIVEL_TIPO_PRODUTO T,'
      '       NIVEL_PRODUTO P'
      ' WHERE T.ID_NIVEL_PRODUTO = P.ID_NIVEL_PRODUTO'
      '   AND T.ID_TIPO_PRODUTO = :p_id_tipo_produto'
      '  ORDER BY T.SEQUENCIA')
    Optimize = False
    Variables.Data = {
      0400000001000000240000003A0050005F00490044005F005400490050004F00
      5F00500052004F004400550054004F00030000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000000300000020000000490044005F004E004900560045004C005F005000
      52004F004400550054004F000100000000001200000044004500530043005200
      4900430041004F0001000000000012000000530045005100550045004E004300
      49004100010000000000}
    Session = orabanco
    Left = 605
    Top = 24
    object Q_Busca_NivelID_NIVEL_PRODUTO: TFloatField
      FieldName = 'ID_NIVEL_PRODUTO'
      Required = True
    end
    object Q_Busca_NivelDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 30
    end
    object Q_Busca_NivelSEQUENCIA: TFloatField
      FieldName = 'SEQUENCIA'
      Required = True
    end
  end
  object Q_Busca_Nivel_Conteudo: TOracleDataSet
    SQL.Strings = (
      'SELECT C.ID_NIVEL_CONTEUDO, C.DESCRICAO '
      '  FROM PM.NIVEL_CONTEUDO C'
      ' WHERE C.ID_NIVEL_PRODUTO = :P_ID_NIVEL_PRODUTO'
      '   AND C.CONTEUDO = :P_CONTEUDO')
    Optimize = False
    Variables.Data = {
      0400000002000000260000003A0050005F00490044005F004E00490056004500
      4C005F00500052004F004400550054004F000300000000000000000000001600
      00003A0050005F0043004F004E0054004500550044004F000500000000000000
      00000000}
    QBEDefinition.QBEFieldDefs = {
      050000000200000012000000440045005300430052004900430041004F000100
      0000000022000000490044005F004E004900560045004C005F0043004F004E00
      54004500550044004F00010000000000}
    Session = orabanco
    Left = 597
    Top = 80
    object Q_Busca_Nivel_ConteudoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object Q_Busca_Nivel_ConteudoID_NIVEL_CONTEUDO: TFloatField
      FieldName = 'ID_NIVEL_CONTEUDO'
      Required = True
    end
  end
  object orabanco: TOracleSession
    LogonUsername = 'pm'
    LogonPassword = 'fullsql'
    LogonDatabase = 'XE_PRIME'
    Preferences.ConvertUTF = cuUTF8ToUTF16
    Left = 192
    Top = 216
  end
  object OracleDataSet1: TOracleDataSet
    SQL.Strings = (
      
        'select r.rowid, r.id_referencia, r.descricao, r.descricao_etique' +
        'ta, '
      '       r.id_forma, r.foto1, r.foto2, r.foto3, r.id_cor'
      '  from pm.referencia r'
      ' where r.id_referencia like :p_id_referencia')
    Optimize = False
    Variables.Data = {
      0400000001000000200000003A0050005F00490044005F005200450046004500
      520045004E00430049004100050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      050000000800000012000000440045005300430052004900430041004F000100
      0000000024000000440045005300430052004900430041004F005F0045005400
      4900510055004500540041000100000000000A00000046004F0054004F003100
      0000000000000A00000046004F0054004F0032000000000000000A0000004600
      4F0054004F0033000000000000001A000000490044005F005200450046004500
      520045004E004300490041000100000000000C000000490044005F0043004F00
      520001000000000010000000490044005F0046004F0052004D00410001000000
      0000}
    Left = 524
    Top = 280
    object StringField1: TStringField
      DisplayLabel = 'Refer'#234'ncia'
      FieldName = 'ID_REFERENCIA'
      Required = True
      Size = 50
    end
    object StringField2: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      DisplayWidth = 200
      FieldName = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object StringField3: TStringField
      DisplayLabel = 'Desc. Etiqueta'
      FieldName = 'DESCRICAO_ETIQUETA'
      Required = True
      Size = 15
    end
    object BlobField1: TBlobField
      DisplayLabel = 'Foto1'
      FieldName = 'FOTO1'
      BlobType = ftOraBlob
    end
    object BlobField2: TBlobField
      DisplayLabel = 'Foto2'
      FieldName = 'FOTO2'
      BlobType = ftOraBlob
    end
    object BlobField3: TBlobField
      DisplayLabel = 'Foto3'
      FieldName = 'FOTO3'
      BlobType = ftOraBlob
    end
    object StringField4: TStringField
      DisplayLabel = 'Cor'
      FieldName = 'ID_COR'
      Required = True
      Size = 30
    end
    object StringField5: TStringField
      FieldName = 'ID_FORMA'
      Required = True
      Size = 15
    end
  end
  object DataSource2: TDataSource
    Left = 572
    Top = 160
  end
  object DS_Numeros_Grades: TDataSource
    DataSet = MemoryTable
    Left = 412
    Top = 193
  end
  object MemoryTable: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 344
    Top = 296
    Data = {
      A40A00009619E0BD01000000180000004B000000000003000000A40A0A4E554D
      45524143414F310100490000000100055749445448020002000A000A4E554D45
      524143414F320100490000000100055749445448020002000A000A4E554D4552
      4143414F330100490000000100055749445448020002000A000A4E554D455241
      43414F340100490000000100055749445448020002000A000A4E554D45524143
      414F350100490000000100055749445448020002000A000A4E554D4552414341
      4F360100490000000100055749445448020002000A000A4E554D45524143414F
      370100490000000100055749445448020002000A000A4E554D45524143414F38
      0100490000000100055749445448020002000A000A4E554D45524143414F3901
      00490000000100055749445448020002000A000B4E554D45524143414F313001
      00490000000100055749445448020002000A000B4E554D45524143414F313101
      00490000000100055749445448020002000A000B4E554D45524143414F313201
      00490000000100055749445448020002000A000B4E554D45524143414F313301
      00490000000100055749445448020002000A000B4E554D45524143414F313401
      00490000000100055749445448020002000A000B4E554D45524143414F313501
      00490000000100055749445448020002000A000B4E554D45524143414F313601
      00490000000100055749445448020002000A000B4E554D45524143414F313701
      00490000000100055749445448020002000A000B4E554D45524143414F313801
      00490000000100055749445448020002000A000B4E554D45524143414F313901
      00490000000100055749445448020002000A000B4E554D45524143414F323001
      00490000000100055749445448020002000A000B4E554D45524143414F323101
      00490000000100055749445448020002000A000B4E554D45524143414F323201
      00490000000100055749445448020002000A000B4E554D45524143414F323301
      00490000000100055749445448020002000A000B4E554D45524143414F323401
      00490000000100055749445448020002000A00105354415455534E554D455241
      43414F310100490000000100055749445448020002000100105354415455534E
      554D45524143414F320100490000000100055749445448020002000100105354
      415455534E554D45524143414F33010049000000010005574944544802000200
      0100105354415455534E554D45524143414F3401004900000001000557494454
      48020002000100105354415455534E554D45524143414F350100490000000100
      055749445448020002000100105354415455534E554D45524143414F36010049
      0000000100055749445448020002000100105354415455534E554D4552414341
      4F370100490000000100055749445448020002000100105354415455534E554D
      45524143414F3801004900000001000557494454480200020001001053544154
      55534E554D45524143414F390100490000000100055749445448020002000100
      115354415455534E554D45524143414F31300100490000000100055749445448
      020002000100115354415455534E554D45524143414F31310100490000000100
      055749445448020002000100115354415455534E554D45524143414F31320100
      490000000100055749445448020002000100115354415455534E554D45524143
      414F31330100490000000100055749445448020002000100115354415455534E
      554D45524143414F313401004900000001000557494454480200020001001153
      54415455534E554D45524143414F313501004900000001000557494454480200
      02000100115354415455534E554D45524143414F313601004900000001000557
      49445448020002000100115354415455534E554D45524143414F313701004900
      00000100055749445448020002000100115354415455534E554D45524143414F
      31380100490000000100055749445448020002000100115354415455534E554D
      45524143414F3139010049000000010005574944544802000200010011535441
      5455534E554D45524143414F3230010049000000010005574944544802000200
      0100115354415455534E554D45524143414F3231010049000000010005574944
      5448020002000100115354415455534E554D45524143414F3232010049000000
      0100055749445448020002000100115354415455534E554D45524143414F3233
      0100490000000100055749445448020002000100115354415455534E554D4552
      4143414F32340100490000000100055749445448020002000100115649534956
      454C4E554D45524143414F310100490000000100055749445448020002000100
      115649534956454C4E554D45524143414F320100490000000100055749445448
      020002000100115649534956454C4E554D45524143414F330100490000000100
      055749445448020002000100115649534956454C4E554D45524143414F340100
      490000000100055749445448020002000100115649534956454C4E554D455241
      43414F350100490000000100055749445448020002000100115649534956454C
      4E554D45524143414F3601004900000001000557494454480200020001001156
      49534956454C4E554D45524143414F3701004900000001000557494454480200
      02000100115649534956454C4E554D45524143414F3801004900000001000557
      49445448020002000100115649534956454C4E554D45524143414F3901004900
      00000100055749445448020002000100125649534956454C4E554D4552414341
      4F31300100490000000100055749445448020002000100125649534956454C4E
      554D45524143414F313101004900000001000557494454480200020001001256
      49534956454C4E554D45524143414F3132010049000000010005574944544802
      0002000100125649534956454C4E554D45524143414F31330100490000000100
      055749445448020002000100125649534956454C4E554D45524143414F313401
      00490000000100055749445448020002000100125649534956454C4E554D4552
      4143414F31350100490000000100055749445448020002000100125649534956
      454C4E554D45524143414F313601004900000001000557494454480200020001
      00125649534956454C4E554D45524143414F3137010049000000010005574944
      5448020002000100125649534956454C4E554D45524143414F31380100490000
      000100055749445448020002000100125649534956454C4E554D45524143414F
      31390100490000000100055749445448020002000100125649534956454C4E55
      4D45524143414F32300100490000000100055749445448020002000100125649
      534956454C4E554D45524143414F323101004900000001000557494454480200
      02000100125649534956454C4E554D45524143414F3232010049000000010005
      5749445448020002000100125649534956454C4E554D45524143414F32330100
      490000000100055749445448020002000100125649534956454C4E554D455241
      43414F323401004900000001000557494454480200020001000B4E554D455241
      43414F32350100490000000100055749445448020002000A0011535441545553
      4E554D45524143414F3235010049000000010005574944544802000200010012
      5649534956454C4E554D45524143414F32350100490000000100055749445448
      0200020001000000}
    object MemoryTableNUMERACAO1: TStringField
      FieldName = 'NUMERACAO1'
      Size = 10
    end
    object MemoryTableNUMERACAO2: TStringField
      FieldName = 'NUMERACAO2'
      Size = 10
    end
    object MemoryTableNUMERACAO3: TStringField
      FieldName = 'NUMERACAO3'
      Size = 10
    end
    object MemoryTableNUMERACAO4: TStringField
      FieldName = 'NUMERACAO4'
      Size = 10
    end
    object MemoryTableNUMERACAO5: TStringField
      FieldName = 'NUMERACAO5'
      Size = 10
    end
    object MemoryTableNUMERACAO6: TStringField
      FieldName = 'NUMERACAO6'
      Size = 10
    end
    object MemoryTableNUMERACAO7: TStringField
      FieldName = 'NUMERACAO7'
      Size = 10
    end
    object MemoryTableNUMERACAO8: TStringField
      FieldName = 'NUMERACAO8'
      Size = 10
    end
    object MemoryTableNUMERACAO9: TStringField
      FieldName = 'NUMERACAO9'
      Size = 10
    end
    object MemoryTableNUMERACAO10: TStringField
      FieldName = 'NUMERACAO10'
      Size = 10
    end
    object MemoryTableNUMERACAO11: TStringField
      FieldName = 'NUMERACAO11'
      Size = 10
    end
    object MemoryTableNUMERACAO12: TStringField
      FieldName = 'NUMERACAO12'
      Size = 10
    end
    object MemoryTableNUMERACAO13: TStringField
      FieldName = 'NUMERACAO13'
      Size = 10
    end
    object MemoryTableNUMERACAO14: TStringField
      FieldName = 'NUMERACAO14'
      Size = 10
    end
    object MemoryTableNUMERACAO15: TStringField
      FieldName = 'NUMERACAO15'
      Size = 10
    end
    object MemoryTableNUMERACAO16: TStringField
      FieldName = 'NUMERACAO16'
      Size = 10
    end
    object MemoryTableNUMERACAO17: TStringField
      FieldName = 'NUMERACAO17'
      Size = 10
    end
    object MemoryTableNUMERACAO18: TStringField
      FieldName = 'NUMERACAO18'
      Size = 10
    end
    object MemoryTableNUMERACAO19: TStringField
      FieldName = 'NUMERACAO19'
      Size = 10
    end
    object MemoryTableNUMERACAO20: TStringField
      FieldName = 'NUMERACAO20'
      Size = 10
    end
    object MemoryTableNUMERACAO21: TStringField
      FieldName = 'NUMERACAO21'
      Size = 10
    end
    object MemoryTableNUMERACAO22: TStringField
      FieldName = 'NUMERACAO22'
      Size = 10
    end
    object MemoryTableNUMERACAO23: TStringField
      FieldName = 'NUMERACAO23'
      Size = 10
    end
    object MemoryTableNUMERACAO24: TStringField
      FieldName = 'NUMERACAO24'
      Size = 10
    end
    object MemoryTableSTATUSNUMERACAO1: TStringField
      FieldName = 'STATUSNUMERACAO1'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO2: TStringField
      FieldName = 'STATUSNUMERACAO2'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO3: TStringField
      FieldName = 'STATUSNUMERACAO3'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO4: TStringField
      FieldName = 'STATUSNUMERACAO4'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO5: TStringField
      FieldName = 'STATUSNUMERACAO5'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO6: TStringField
      FieldName = 'STATUSNUMERACAO6'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO7: TStringField
      FieldName = 'STATUSNUMERACAO7'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO8: TStringField
      FieldName = 'STATUSNUMERACAO8'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO9: TStringField
      FieldName = 'STATUSNUMERACAO9'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO10: TStringField
      FieldName = 'STATUSNUMERACAO10'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO11: TStringField
      FieldName = 'STATUSNUMERACAO11'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO12: TStringField
      FieldName = 'STATUSNUMERACAO12'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO13: TStringField
      FieldName = 'STATUSNUMERACAO13'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO14: TStringField
      FieldName = 'STATUSNUMERACAO14'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO15: TStringField
      FieldName = 'STATUSNUMERACAO15'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO16: TStringField
      FieldName = 'STATUSNUMERACAO16'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO17: TStringField
      FieldName = 'STATUSNUMERACAO17'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO18: TStringField
      FieldName = 'STATUSNUMERACAO18'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO19: TStringField
      FieldName = 'STATUSNUMERACAO19'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO20: TStringField
      FieldName = 'STATUSNUMERACAO20'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO21: TStringField
      FieldName = 'STATUSNUMERACAO21'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO22: TStringField
      FieldName = 'STATUSNUMERACAO22'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO23: TStringField
      FieldName = 'STATUSNUMERACAO23'
      Size = 1
    end
    object MemoryTableSTATUSNUMERACAO24: TStringField
      FieldName = 'STATUSNUMERACAO24'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO1: TStringField
      FieldName = 'VISIVELNUMERACAO1'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO2: TStringField
      FieldName = 'VISIVELNUMERACAO2'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO3: TStringField
      FieldName = 'VISIVELNUMERACAO3'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO4: TStringField
      FieldName = 'VISIVELNUMERACAO4'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO5: TStringField
      FieldName = 'VISIVELNUMERACAO5'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO6: TStringField
      FieldName = 'VISIVELNUMERACAO6'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO7: TStringField
      FieldName = 'VISIVELNUMERACAO7'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO8: TStringField
      FieldName = 'VISIVELNUMERACAO8'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO9: TStringField
      FieldName = 'VISIVELNUMERACAO9'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO10: TStringField
      FieldName = 'VISIVELNUMERACAO10'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO11: TStringField
      FieldName = 'VISIVELNUMERACAO11'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO12: TStringField
      FieldName = 'VISIVELNUMERACAO12'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO13: TStringField
      FieldName = 'VISIVELNUMERACAO13'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO14: TStringField
      FieldName = 'VISIVELNUMERACAO14'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO15: TStringField
      FieldName = 'VISIVELNUMERACAO15'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO16: TStringField
      FieldName = 'VISIVELNUMERACAO16'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO17: TStringField
      FieldName = 'VISIVELNUMERACAO17'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO18: TStringField
      FieldName = 'VISIVELNUMERACAO18'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO19: TStringField
      FieldName = 'VISIVELNUMERACAO19'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO20: TStringField
      FieldName = 'VISIVELNUMERACAO20'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO21: TStringField
      FieldName = 'VISIVELNUMERACAO21'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO22: TStringField
      FieldName = 'VISIVELNUMERACAO22'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO23: TStringField
      FieldName = 'VISIVELNUMERACAO23'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO24: TStringField
      FieldName = 'VISIVELNUMERACAO24'
      Size = 1
    end
    object MemoryTableNUMERACAO25: TStringField
      FieldName = 'NUMERACAO25'
      Size = 10
    end
    object MemoryTableSTATUSNUMERACAO25: TStringField
      FieldName = 'STATUSNUMERACAO25'
      Size = 1
    end
    object MemoryTableVISIVELNUMERACAO25: TStringField
      FieldName = 'VISIVELNUMERACAO25'
      Size = 1
    end
  end
end
