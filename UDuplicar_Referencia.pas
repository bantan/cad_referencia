unit UDuplicar_Referencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, DBCtrls, DB, OracleData, ComCtrls, ExtCtrls, Oracle;

type
  TFrm_Duplicar_Referencia = class(TForm)
    Label1: TLabel;
    Label6: TLabel;
    Spd_Refer_Origem: TSpeedButton;
    ED_REFER_ORIGEM: TEdit;
    ED_REFER_ORIGEM_DESC: TEdit;
    ED_REFER_DESTINO: TEdit;
    ED_REFER_DESTINO_DESC: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Q_Referencia: TOracleDataSet;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    Q_Insere_Nova_Referencia: TOracleDataSet;
    Q_ReferenciaREFERENCIA: TStringField;
    Q_ReferenciaREFERENCIA_BASE: TStringField;
    Q_ReferenciaDESCRICAO: TStringField;
    Q_ReferenciaDESCRICAO_ETIQUETA: TStringField;
    Q_ReferenciaFORMA: TStringField;
    Q_ReferenciaFOTO1: TBlobField;
    Q_ReferenciaFOTO2: TBlobField;
    Q_ReferenciaFOTO3: TBlobField;
    Q_ReferenciaCOR: TStringField;
    Q_ReferenciaCODIGO_LINHA_PRODUTO: TIntegerField;
    Q_ReferenciaTIPO_MATERIAL: TIntegerField;
    Q_ReferenciaTIPO_SOLADO: TIntegerField;
    Q_ReferenciaTIPO_CORTE: TIntegerField;
    Q_ReferenciaNUMERO_BASE: TStringField;
    Q_ReferenciaMENOR_NUMERO: TStringField;
    Q_ReferenciaMAIOR_NUMERO: TStringField;
    Q_ReferenciaREFERENCIA_BASE_PAGTO: TStringField;
    Q_ReferenciaPLANEJA: TStringField;
    Q_ReferenciaCNPJ_CPF_CLIENTE: TStringField;
    Q_ReferenciaCODIGO_DESIGNER: TIntegerField;
    Q_ReferenciaCODIGO_MODELISTA: TIntegerField;
    Q_ReferenciaEVENTO_LANCAMENTO: TIntegerField;
    Q_ReferenciaCOLECAO_LANCAMENTO: TIntegerField;
    Q_ReferenciaOBSERVACAO: TStringField;
    Q_ReferenciaATIVO: TStringField;
    procedure ED_REFER_ORIGEMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_OkClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ED_REFER_ORIGEMDblClick(Sender: TObject);
    procedure ED_REFER_ORIGEMExit(Sender: TObject);
    procedure ED_REFER_DESTINOKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    p_referencia: string;
    m_diretorio_sistema : String;

    fecha: Boolean;
  end;

var
  Frm_Duplicar_Referencia: TFrm_Duplicar_Referencia;

implementation

uses UCad_Referencia, UCons_Referencia, Unit_funcoes;

{$R *.dfm}

procedure TFrm_Duplicar_Referencia.btn_CancelarClick(Sender: TObject);
begin
    fecha := true;
end;

procedure TFrm_Duplicar_Referencia.btn_OkClick(Sender: TObject);
begin

//    if ED_REFER_ORIGEM.Text = EmptyStr then
//    begin
//        MessageDlg('� necess�rio informar a refer�ncia de origem!',mtInformation,[mbok],0);
//        ED_REFER_ORIGEM.SetFocus;
//        Abort;
//    end;
//
//    if ED_REFER_DESTINO.Text = EmptyStr then
//    begin
//        MessageDlg('� necess�rio informar a nova refer�ncia!',mtInformation,[mbok],0);
//        ED_REFER_DESTINO.SetFocus;
//        Abort;
//    end;
//
//    Q_Referencia.close;
//    Q_Referencia.SetVariable('p_referencia',ED_REFER_ORIGEM.Text);
//    Q_Referencia.open;
//    if Q_Referencia.RecordCount > 0 then
//    begin
//        FrmPrincipal.OraBanco.SetTransaction(tmReadCommitted);
//        try
//            Q_Insere_Nova_Referencia.close;
//            Q_Insere_Nova_Referencia.SetVariable('p_referencia',ED_REFER_DESTINO.Text);
//            Q_Insere_Nova_Referencia.SetVariable('p_referencia_base',ED_REFER_DESTINO.Text);
//            if ED_REFER_DESTINO_DESC.Text <> EmptyStr then
//              Q_Insere_Nova_Referencia.SetVariable('p_descricao',ED_REFER_DESTINO_DESC.Text)
//            else
//              Q_Insere_Nova_Referencia.SetVariable('p_descricao',Q_ReferenciaDESCRICAO.value);
//
//            Q_Insere_Nova_Referencia.SetVariable('p_descricao_etiqueta',Q_ReferenciaDESCRICAO_ETIQUETA.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_forma',Q_ReferenciaFORMA.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_codigo_linha_produto',Q_ReferenciaCODIGO_LINHA_PRODUTO.value);
////            Q_Insere_Nova_Referencia.SetVariable('p_foto1',Q_ReferenciaFOTO1.value);
////            Q_Insere_Nova_Referencia.SetVariable('p_foto2',Q_ReferenciaFOTO2.value);
////            Q_Insere_Nova_Referencia.SetVariable('p_foto3',Q_ReferenciaFOTO3.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_cor',Q_ReferenciaCOR.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_tipo_material',Q_ReferenciaTIPO_MATERIAL.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_tipo_solado',Q_ReferenciaTIPO_SOLADO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_tipo_corte',Q_ReferenciaTIPO_CORTE.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_numero_base',Q_ReferenciaNUMERO_BASE.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_menor_numero',Q_ReferenciaMENOR_NUMERO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_maior_numero',Q_ReferenciaMAIOR_NUMERO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_referencia_base_pagto',Q_ReferenciaREFERENCIA_BASE_PAGTO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_planeja',Q_ReferenciaPLANEJA.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_cnpj_cpf_cliente',Q_ReferenciaCNPJ_CPF_CLIENTE.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_colecao_lancamento',Q_ReferenciaCOLECAO_LANCAMENTO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_evento_lancamento',Q_ReferenciaEVENTO_LANCAMENTO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_codigo_designer',Q_ReferenciaCODIGO_DESIGNER.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_codigo_modelista',Q_ReferenciaCODIGO_MODELISTA.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_observacao',Q_ReferenciaOBSERVACAO.value);
//            Q_Insere_Nova_Referencia.SetVariable('p_referencia_origem',ED_REFER_ORIGEM.Text);
//            Q_Insere_Nova_Referencia.SetVariable('p_ativo',Q_ReferenciaATIVO.value);
//            Q_Insere_Nova_Referencia.ExecSQL;
//
//            FrmPrincipal.OraBanco.ApplyUpdates([Q_Insere_Nova_Referencia],False);
//            FrmPrincipal.OraBanco.Commit;
//            MessageDlg('A refer�ncia '+ED_REFER_ORIGEM.Text+' foi duplicada para '+ED_REFER_DESTINO.Text+' com sucesso!',mtInformation,[mbok],0);
//        except
//            FrmPrincipal.OraBanco.Rollback;
//            raise;
//        end;
//    end;
//    fecha := true;
end;

procedure TFrm_Duplicar_Referencia.ED_REFER_DESTINOKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #45 then
    key := #0;
end;

procedure TFrm_Duplicar_Referencia.ED_REFER_ORIGEMDblClick(Sender: TObject);
begin
//  Frm_Cons_Referencia := TFrm_Cons_Referencia.create(self);
//  Frm_Cons_Referencia.P_Sessao            := FrmPrincipal.OraBanco;
//  Frm_Cons_Referencia.p_campo_default     := 'REFERENCIA';
//  Frm_Cons_Referencia.p_user              := FrmPrincipal.conexao.User;
//  Frm_Cons_Referencia.p_password          := FrmPrincipal.conexao.Password;
//  Frm_Cons_Referencia.m_diretorio_sistema := FrmPrincipal.conexao.Diretorio_sistema;
//
//  if Frm_Cons_Referencia.ShowModal = mrOk then
//  begin
//      //ED_REFER_ORIGEM.Text      := Frm_Cons_Referencia.Q_Cons_ReferenciaREFERENCIA.Value;    mateus
//      //ED_REFER_ORIGEM_DESC.Text := Frm_Cons_Referencia.Q_Cons_ReferenciaDESCRICAO.Value;     mateus
//      ED_REFER_DESTINO.SetFocus;
//  end;
end;

procedure TFrm_Duplicar_Referencia.ED_REFER_ORIGEMExit(Sender: TObject);
begin
    if ED_REFER_ORIGEM.Text <> EmptyStr then
    begin
        Q_Referencia.close;
        Q_Referencia.SetVariable('p_referencia',ED_REFER_ORIGEM.Text);
        Q_Referencia.Open;
        if Q_Referencia.RecordCount > 0 then
          ED_REFER_ORIGEM_DESC.Text := Q_ReferenciaDESCRICAO.asstring
        else
        begin
            MessageDlg('Refer�ncia n�o encontrada!',mtinformation,[mbok],0);
            ED_REFER_ORIGEM.SetFocus;
            Abort;
        end;

    end;
end;

procedure TFrm_Duplicar_Referencia.ED_REFER_ORIGEMKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if key = vk_f8 then
      (sender as TEdit).OnDblClick(self);
end;

procedure TFrm_Duplicar_Referencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    If Not fecha Then
      Action := caNone
    Else
      Action := caFree;
end;

procedure TFrm_Duplicar_Referencia.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If GetStateK (VK_LMENU) And (Key = VK_F4) Then
      fecha := False;
end;

procedure TFrm_Duplicar_Referencia.FormShow(Sender: TObject);
begin
    Image_Empresa.Picture.LoadFromFile(m_diretorio_sistema+'Imagens\logo_empresa.ico');
    Image_Prime.Picture.LoadFromFile(m_diretorio_sistema+'Imagens\logo_prime.ico');
    ED_REFER_ORIGEM.SetFocus;
    ED_REFER_DESTINO.ReadOnly := False;
    if p_referencia <> EmptyStr then
    begin
        ED_REFER_ORIGEM.Text := p_referencia;
        ED_REFER_ORIGEMExit(self);
    end;
end;

end.
