unit UDM;

interface

uses
  System.SysUtils, System.Classes,
  Data.DB, OracleData, Oracle, RxMemDS, Datasnap.DBClient;

type
  TDM = class(TDataModule)


    Ds_Referencia: TDataSource;
    Ds_Cons_Referencia: TDataSource;
    Q_Cons_Referencia_Nivel: TOracleDataSet;
    Q_Cons_Referencia_NivelCONTEUDO: TStringField;
    Q_Cons_Referencia_NivelDESCRICAO: TStringField;
    Q_Cons_Referencia_NivelID_NIVEL_PRODUTO: TFloatField;
    Q_Referencia_Nivel: TOracleDataSet;
    Q_Referencia_NivelID_REFERENCIA_NIVEL: TFloatField;
    Q_Referencia_NivelID_REFERENCIA: TStringField;
    Q_Referencia_NivelID_NIVEL_CONTEUDO: TFloatField;
    Q_Referencia: TOracleDataSet;
    Q_ReferenciaCliente_Razao_Social: TStringField;
    Q_ReferenciaForma_Descricao: TStringField;
    Q_ReferenciaTipo_Material_Descricao: TStringField;
    Q_ReferenciaTipo_Solado_Descricao: TStringField;
    Q_ReferenciaTipo_Corte_Descricao: TStringField;
    Q_ReferenciaID_REFERENCIA: TStringField;
    Q_ReferenciaDESCRICAO: TStringField;
    Q_ReferenciaDESCRICAO_ETIQUETA: TStringField;
    Q_ReferenciaFOTO1: TBlobField;
    Q_ReferenciaFOTO2: TBlobField;
    Q_ReferenciaFOTO3: TBlobField;
    Q_ReferenciaNUMERO_BASE: TStringField;
    Q_ReferenciaID_COR: TStringField;
    Q_ReferenciaID_TIPO_MATERIAL: TFloatField;
    Q_ReferenciaID_TIPO_SOLADO: TFloatField;
    Q_ReferenciaID_TIPO_CORTE: TFloatField;
    Q_ReferenciaDescricao_Evento: TStringField;
    Q_ReferenciaID_COLECAO: TFloatField;
    Q_ReferenciaID_EVENTO: TFloatField;
    Q_ReferenciaID_LINHA_PRODUTO: TFloatField;
    Q_ReferenciaNome_Designer: TStringField;
    Q_ReferenciaNome_Modelista: TStringField;
    Q_ReferenciaID_DESIGNER: TFloatField;
    Q_ReferenciaID_MODELISTA: TFloatField;
    Q_ReferenciaOBSERVACAO: TStringField;
    Q_ReferenciaVIGENCIA: TDateTimeField;
    Q_ReferenciaID_TIPO_PRODUTO: TFloatField;
    Q_ReferenciaDescricao_Colecao: TStringField;
    Q_ReferenciaTipo_Produto_Descricao: TStringField;
    Q_ReferenciaID_FORMA: TStringField;
    Q_ReferenciaID_CLIENTE: TFloatField;
    Q_ReferenciaID_GRADE: TFloatField;
    Q_Integra_Ref_Produto: TOracleDataSet;
    Q_Integra_Ref_ProdutoID_PRODUTO: TFloatField;
    Q_Integra_Ref_ProdutoCODIGO: TStringField;
    Q_Integra_Ref_ProdutoDESCRICAO: TStringField;
    Q_Integra_Ref_ProdutoID_UNIDADE_PRODUCAO: TStringField;
    Q_Integra_Ref_ProdutoID_UNIDADE_VENDA: TStringField;
    Q_Integra_Ref_ProdutoID_SUB_GRUPO: TFloatField;
    Q_Integra_Ref_ProdutoID_GRADE: TFloatField;
    Q_Integra_Ref_ProdutoID_PRODUTO_RELACIONADO: TFloatField;
    Q_Integra_Ref_ProdutoFATOR_CONVER_COMPRA_PRODUCAO: TFloatField;
    Q_Integra_Ref_ProdutoFATOR_CONVER_PRODUCAO_VENDA: TFloatField;
    Q_Integra_Ref_ProdutoPERC_ACEITE_ENTRADA_COMPRA: TFloatField;
    Q_Integra_Ref_ProdutoPERC_ACEITE_PEDIDO_COMPRA: TFloatField;
    Q_Integra_Ref_ProdutoPERC_ACEITE_SAIDA_CONSUMO: TFloatField;
    Q_Integra_Ref_ProdutoINSTRUCAO_RECEBIMENTO: TStringField;
    Q_Integra_Ref_ProdutoATIVO: TStringField;
    Q_Integra_Ref_ProdutoEAN14: TStringField;
    Q_Integra_Ref_Produto_Numero: TOracleDataSet;
    Q_Integra_Ref_Produto_NumeroID_PRODUTO_NUMERO: TFloatField;
    Q_Integra_Ref_Produto_NumeroID_PRODUTO: TFloatField;
    Q_Integra_Ref_Produto_NumeroPOSICAO: TIntegerField;
    Q_Integra_Ref_Produto_NumeroNUMERO: TStringField;
    Q_Integra_Ref_Produto_NumeroEAN13: TStringField;
    Q_Integra_Ref_Produto_NumeroATIVO: TStringField;
    Q_Referencia_Produto_Numero: TOracleDataSet;
    Q_Referencia_Produto_NumeroID_PRODUTO_NUMERO: TFloatField;
    Q_Referencia_Produto_NumeroID_PRODUTO: TFloatField;
    Q_Referencia_Produto_NumeroPOSICAO: TIntegerField;
    Q_Referencia_Produto_NumeroNUMERO: TStringField;
    Q_Referencia_Produto_NumeroEAN13: TStringField;
    Q_Referencia_Produto_NumeroATIVO: TStringField;
    Q_Cons_Referencia: TOracleDataSet;
    Q_Cons_ReferenciaID_REFERENCIA: TStringField;
    Q_Cons_ReferenciaDESCRICAO: TStringField;
    Q_Cons_ReferenciaDESCRICAO_ETIQUETA: TStringField;
    Q_Cons_ReferenciaFOTO1: TBlobField;
    Q_Cons_ReferenciaFOTO2: TBlobField;
    Q_Cons_ReferenciaFOTO3: TBlobField;
    Q_Cons_ReferenciaID_COR: TStringField;
    Q_Cons_ReferenciaID_FORMA: TStringField;
    Q_Busca_Nivel: TOracleDataSet;
    Q_Busca_NivelID_NIVEL_PRODUTO: TFloatField;
    Q_Busca_NivelDESCRICAO: TStringField;
    Q_Busca_NivelSEQUENCIA: TFloatField;
    Q_Busca_Nivel_Conteudo: TOracleDataSet;
    Q_Busca_Nivel_ConteudoDESCRICAO: TStringField;
    Q_Busca_Nivel_ConteudoID_NIVEL_CONTEUDO: TFloatField;
    orabanco: TOracleSession;
    OracleDataSet1: TOracleDataSet;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    BlobField1: TBlobField;
    BlobField2: TBlobField;
    BlobField3: TBlobField;
    StringField4: TStringField;
    StringField5: TStringField;
    DataSource2: TDataSource;
    DS_Numeros_Grades: TDataSource;
    MemoryTable: TClientDataSet;
    MemoryTableNUMERACAO1: TStringField;
    MemoryTableNUMERACAO2: TStringField;
    MemoryTableNUMERACAO3: TStringField;
    MemoryTableNUMERACAO4: TStringField;
    MemoryTableNUMERACAO5: TStringField;
    MemoryTableNUMERACAO6: TStringField;
    MemoryTableNUMERACAO7: TStringField;
    MemoryTableNUMERACAO8: TStringField;
    MemoryTableNUMERACAO9: TStringField;
    MemoryTableNUMERACAO10: TStringField;
    MemoryTableNUMERACAO11: TStringField;
    MemoryTableNUMERACAO12: TStringField;
    MemoryTableNUMERACAO13: TStringField;
    MemoryTableNUMERACAO14: TStringField;
    MemoryTableNUMERACAO15: TStringField;
    MemoryTableNUMERACAO16: TStringField;
    MemoryTableNUMERACAO17: TStringField;
    MemoryTableNUMERACAO18: TStringField;
    MemoryTableNUMERACAO19: TStringField;
    MemoryTableNUMERACAO20: TStringField;
    MemoryTableNUMERACAO21: TStringField;
    MemoryTableNUMERACAO22: TStringField;
    MemoryTableNUMERACAO23: TStringField;
    MemoryTableNUMERACAO24: TStringField;
    MemoryTableSTATUSNUMERACAO1: TStringField;
    MemoryTableSTATUSNUMERACAO2: TStringField;
    MemoryTableSTATUSNUMERACAO3: TStringField;
    MemoryTableSTATUSNUMERACAO4: TStringField;
    MemoryTableSTATUSNUMERACAO5: TStringField;
    MemoryTableSTATUSNUMERACAO6: TStringField;
    MemoryTableSTATUSNUMERACAO7: TStringField;
    MemoryTableSTATUSNUMERACAO8: TStringField;
    MemoryTableSTATUSNUMERACAO9: TStringField;
    MemoryTableSTATUSNUMERACAO10: TStringField;
    MemoryTableSTATUSNUMERACAO11: TStringField;
    MemoryTableSTATUSNUMERACAO12: TStringField;
    MemoryTableSTATUSNUMERACAO13: TStringField;
    MemoryTableSTATUSNUMERACAO14: TStringField;
    MemoryTableSTATUSNUMERACAO15: TStringField;
    MemoryTableSTATUSNUMERACAO16: TStringField;
    MemoryTableSTATUSNUMERACAO17: TStringField;
    MemoryTableSTATUSNUMERACAO18: TStringField;
    MemoryTableSTATUSNUMERACAO19: TStringField;
    MemoryTableSTATUSNUMERACAO20: TStringField;
    MemoryTableSTATUSNUMERACAO21: TStringField;
    MemoryTableSTATUSNUMERACAO22: TStringField;
    MemoryTableSTATUSNUMERACAO23: TStringField;
    MemoryTableSTATUSNUMERACAO24: TStringField;
    MemoryTableVISIVELNUMERACAO1: TStringField;
    MemoryTableVISIVELNUMERACAO2: TStringField;
    MemoryTableVISIVELNUMERACAO3: TStringField;
    MemoryTableVISIVELNUMERACAO4: TStringField;
    MemoryTableVISIVELNUMERACAO5: TStringField;
    MemoryTableVISIVELNUMERACAO6: TStringField;
    MemoryTableVISIVELNUMERACAO7: TStringField;
    MemoryTableVISIVELNUMERACAO8: TStringField;
    MemoryTableVISIVELNUMERACAO9: TStringField;
    MemoryTableVISIVELNUMERACAO10: TStringField;
    MemoryTableVISIVELNUMERACAO11: TStringField;
    MemoryTableVISIVELNUMERACAO12: TStringField;
    MemoryTableVISIVELNUMERACAO13: TStringField;
    MemoryTableVISIVELNUMERACAO14: TStringField;
    MemoryTableVISIVELNUMERACAO15: TStringField;
    MemoryTableVISIVELNUMERACAO16: TStringField;
    MemoryTableVISIVELNUMERACAO17: TStringField;
    MemoryTableVISIVELNUMERACAO18: TStringField;
    MemoryTableVISIVELNUMERACAO19: TStringField;
    MemoryTableVISIVELNUMERACAO20: TStringField;
    MemoryTableVISIVELNUMERACAO21: TStringField;
    MemoryTableVISIVELNUMERACAO22: TStringField;
    MemoryTableVISIVELNUMERACAO23: TStringField;
    MemoryTableVISIVELNUMERACAO24: TStringField;
    MemoryTableNUMERACAO25: TStringField;
    MemoryTableSTATUSNUMERACAO25: TStringField;
    MemoryTableVISIVELNUMERACAO25: TStringField;
    Q_ReferenciaDESCRICAO_FATURAMENTO: TStringField;
    Q_ReferenciaID_UNIDADE: TStringField;
    Q_ReferenciaID_NCM: TStringField;
    Q_ReferenciaPESO_BRUTO: TFloatField;
    Q_ReferenciaPESO_LIQUIDO: TFloatField;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
