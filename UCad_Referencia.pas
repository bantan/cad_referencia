unit UCad_Referencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DosMove, ActnList, Buttons, ExtCtrls, FormTools,
  DB, Conexao, StdCtrls, DBClient, Mask, USobreSistema,
  DBCtrls, OracleData, Oracle, shellapi, DBGrids, Jpeg,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid, ppBands,
  ppClass, ppCtrls, ppVar, ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppComm,
  ppRelatv, ppDBPipe, ppDBBDE, Grids, ExtDlgs, cxCheckBox, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, System.Actions,
  Bde.DBTables, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, RxMemDS, RxToolEdit,
  RxDBCtrl;

type
  TFrmPrincipal = class(TForm)
    PanTopo: TPanel;
    Shape2: TShape;
    SpCancelar: TSpeedButton;
    SpNovo: TSpeedButton;
    SpExcluir: TSpeedButton;
    SpImprimir: TSpeedButton;
    SpGravar: TSpeedButton;
    SpLocalizar: TSpeedButton;
    ActionList: TActionList;
    ActAjuda: TAction;
    ActNovo: TAction;
    ActGravar: TAction;
    ActCancelar: TAction;
    ActExcluir: TAction;
    ActLocalizar: TAction;
    ActImprimir: TAction;
    ActEditar: TAction;
    SpEditar: TSpeedButton;
    PageControl1: TPageControl;
    Tab_Cadastro: TTabSheet;
    Tab_Consulta: TTabSheet;
    Label_Status: TLabel;
    StatusBar2: TStatusBar;
    Panel_Alto: TPanel;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_Consulta: TcxGrid;
    cxGrid_ConsultaDBTableView1ID_REFERENCIA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1FORMA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1COR: TcxGridDBColumn;
    OpenPictureDialog: TOpenPictureDialog;
    pn_imagens: TPanel;
    Image1: TImage;
    Spd_Busca_Foto1: TSpeedButton;
    Spd_Limpa_Foto1: TSpeedButton;
    Label10: TLabel;
    Label11: TLabel;
    Image2: TImage;
    Spd_Limpa_Foto2: TSpeedButton;
    Spd_Busca_Foto2: TSpeedButton;
    Label12: TLabel;
    Image3: TImage;
    Spd_Limpa_Foto3: TSpeedButton;
    Spd_Busca_Foto3: TSpeedButton;
    Panel2: TPanel;
    pn_cabecalho: TPanel;
    Label9: TLabel;
    Spd_Tipo_Produto: TSpeedButton;
    Label1: TLabel;
    Bevel4: TBevel;
    Label26: TLabel;
    spVisualizarGrade: TSpeedButton;
    DBTIPO_PRODUTO: TDBEdit;
    DBTipo_Produto_nome: TEdit;
    DBID_REFERENCIA: TDBEdit;
    RgPossuiGrade: TRadioGroup;
    pn_niveis: TPanel;
    Label2: TLabel;
    ScrollBox_Niveis: TScrollBox;
    pn_descricao: TPanel;
    Label7: TLabel;
    DBDESCRICAO: TDBEdit;
    DBDESCRICAO_ETIQUETA: TDBEdit;
    pn_calcados: TPanel;
    Label14: TLabel;
    Spd_Cor: TSpeedButton;
    Label8: TLabel;
    Spd_Forma: TSpeedButton;
    Label20: TLabel;
    Spd_Tipo_Solado: TSpeedButton;
    Label21: TLabel;
    Spd_Tipo_Corte: TSpeedButton;
    Label32: TLabel;
    Spd_Colecao: TSpeedButton;
    Label33: TLabel;
    Spd_Evento: TSpeedButton;
    Label35: TLabel;
    Spd_Designer: TSpeedButton;
    Label36: TLabel;
    Spd_Modelista: TSpeedButton;
    DBCOR: TDBEdit;
    DBFORMA: TDBEdit;
    DBForma_nome: TEdit;
    DBTIPO_SOLADO: TDBEdit;
    DBTipo_Solado_nome: TEdit;
    DBTIPO_CORTE: TDBEdit;
    DBTipo_Corte_nome: TEdit;
    DBCOLECAO_LANCAMENTO: TDBEdit;
    DBColecao_lancamento_nome: TEdit;
    DBEVENTO_LANCAMENTO: TDBEdit;
    DBEvento_lancamento_nome: TEdit;
    DBCODIGO_DESIGNER: TDBEdit;
    DBCodigo_Designer_nome: TEdit;
    DBCODIGO_MODELISTA: TDBEdit;
    DBCodigo_Modelista_nome: TEdit;
    pn_rodape: TPanel;
    Label16: TLabel;
    Spd_Tipo_Mat_Sol: TSpeedButton;
    Label31: TLabel;
    Spd_Cliente: TSpeedButton;
    Label13: TLabel;
    Label28: TLabel;
    DBTIPO_MATERIAL: TDBEdit;
    DBTipo_Material_nome: TEdit;
    DBOBSERVACAO: TDBMemo;
    ed_cliente: TEdit;
    ed_cliente_nome: TEdit;
    Label6: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    DBDateEdit1: TDBDateEdit;

    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit14KeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Q_ReferenciaCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure ActNovoExecute(Sender: TObject);
    procedure ActGravarExecute(Sender: TObject);
    procedure ActCancelarExecute(Sender: TObject);
    procedure ActExcluirExecute(Sender: TObject);
    procedure ActLocalizarExecute(Sender: TObject);
    procedure ActImprimirExecute(Sender: TObject);
    procedure ActEditarExecute(Sender: TObject);
    procedure ActAjudaExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBTIPO_PRODUTOExit(Sender: TObject);
    procedure DBCORDblClick(Sender: TObject);
    procedure DBFORMAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Spd_FormaClick(Sender: TObject);
    procedure DBFORMADblClick(Sender: TObject);
    procedure DBTIPO_MATERIALKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBTIPO_MATERIALDblClick(Sender: TObject);
    procedure DBTIPO_SOLADODblClick(Sender: TObject);
    procedure DBTIPO_SOLADOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBTIPO_CORTEDblClick(Sender: TObject);
    procedure DBTIPO_CORTEKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure DBCOLECAO_LANCAMENTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCOLECAO_LANCAMENTODblClick(Sender: TObject);
    procedure DBEVENTO_LANCAMENTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure DBEVENTO_LANCAMENTODblClick(Sender: TObject);

    procedure DBCODIGO_DESIGNERKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCODIGO_DESIGNERDblClick(Sender: TObject);

    procedure DBCODIGO_MODELISTAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCODIGO_MODELISTADblClick(Sender: TObject);
    procedure DBTIPO_PRODUTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBTIPO_PRODUTODblClick(Sender: TObject);
    procedure Spd_ClienteClick(Sender: TObject);
    procedure ed_clienteExit(Sender: TObject);
    procedure Spd_Busca_Foto1Click(Sender: TObject);
    procedure Spd_Limpa_Foto1Click(Sender: TObject);
    procedure Spd_Limpa_Foto2Click(Sender: TObject);
    procedure Spd_Busca_Foto2Click(Sender: TObject);
    procedure Spd_Limpa_Foto3Click(Sender: TObject);
    procedure Spd_Busca_Foto3Click(Sender: TObject);
    procedure Image1DblClick(Sender: TObject);
    procedure Image2DblClick(Sender: TObject);
    procedure Image3DblClick(Sender: TObject);
    procedure spVisualizarGradeClick(Sender: TObject);
    procedure RgPossuiGradeExit(Sender: TObject);
    procedure DBCORKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ed_clienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBTIPO_SOLADOExit(Sender: TObject);
    procedure DBFORMAExit(Sender: TObject);
    procedure DBTIPO_CORTEExit(Sender: TObject);
    procedure DBCOLECAO_LANCAMENTOExit(Sender: TObject);
    procedure DBEVENTO_LANCAMENTOExit(Sender: TObject);
    procedure DBCODIGO_DESIGNERExit(Sender: TObject);
    procedure DBCODIGO_MODELISTAExit(Sender: TObject);
    procedure DBTIPO_MATERIALExit(Sender: TObject);

  private

    procedure ExcluiCampos;
    procedure CriaCampos;
    procedure GeraIdReferencia;
    procedure ExecutaOnExit(speed: TObject);
    procedure AbreConsultaNivelConteudo(speed: TObject);
    procedure ExecutaOnKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GravaReferenciaNivel(referencia: String);
    procedure GravaReferenciaProduto(referencia: String; status: String);
    procedure DesabilitaCampos;
    procedure AlimentaCamposNivel();
    procedure AlimentaCampos();

    { Private declarations }
  public
    Conexao: TConexao;
    // variavel criada para armazenar o id do campo empresa
    v_id_empresa: Integer;
    v_id_ncm: Integer;
    procedure Checar_Status_DataSet;
  protected
    tools: TFormTools;
  published
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

uses Unit_Funcoes, UImpressao,
  UVizualizarImagem,
  UDuplicar_Referencia, UCons_Tipo_Produto, UCons_Cor,
  UCons_Nivel_Conteudo, UCons_Forma, UCons_Tipo_Material,
  UCons_Tipo_Solado, UCons_Tipo_Corte, UCons_Colecao, UCons_Evento,
  UCons_Designer, UCons_Modelista, UCons_Empresa, UVisualizar_Grade, UDM;

{$R *.dfm}

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
  tools := TFormTools.Create(self);
  if WindowState = wsMaximized then
    Position := poDefault
  else
    Position := poOwnerFormCenter;
  Conexao := TConexao.Create(DM.OraBanco);
end;

procedure TFrmPrincipal.ActNovoExecute(Sender: TObject);
begin
  if not(SpNovo.Enabled) then
    exit;

  if (Conexao.Inclusao <> 1) then
  begin
    MessageDlg('Voc� n�o tem permiss�o para incluir!', mtInformation,
      [mbok], 0);
    Abort;
  end;

  DM.Q_Referencia.Close;
  DM.Q_Referencia.SetVariable('p_referencia', 0);
  DM.Q_Referencia.Open;
  DM.Q_Referencia.Append;

  ExcluiCampos;
  DM.Q_Referencia_Nivel.Close;
  DM.Q_Referencia_Nivel.SetVariable('p_id_referencia', 0);
  DM.Q_Referencia_Nivel.Open;

  DM.Q_Integra_Ref_Produto.Close;
  DM.Q_Integra_Ref_Produto.SetVariable('p_codigo', 0);
  DM.Q_Integra_Ref_Produto.Open;

  DM.Q_Integra_Ref_Produto_Numero.Close;
  DM.Q_Integra_Ref_Produto_Numero.SetVariable('p_id_produto_numero', 0);
  DM.Q_Integra_Ref_Produto_Numero.Open;

  RgPossuiGrade.ItemIndex := 0;

  DBTIPO_PRODUTO.SetFocus;

  tools.HabilitaComponentes(true, 75);
  tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir,
    SpLocalizar, SpImprimir], [false, false, true, true, false, false, false]);
  Label_Status.Caption := 'Incluindo registro...';
end;

procedure TFrmPrincipal.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    ActLocalizarExecute(self);
end;

procedure TFrmPrincipal.Checar_Status_DataSet;
begin
  if not(DM.Q_Referencia.state in dsEditModes) then
  begin
    MessageDlg('Abra um registro antes!', mtInformation, [mbok], 0);
    Abort;
  end;
end;

procedure TFrmPrincipal.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1ID_REFERENCIA.Focused;
  end;
end;

procedure TFrmPrincipal.DBEdit14KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
end;

procedure TFrmPrincipal.ActEditarExecute(Sender: TObject);
begin
  if not(SpEditar.Enabled) then
    exit;

  if (Conexao.Alteracao <> 1) then
  begin
    MessageDlg('Voc� n�o tem permiss�o para alterar!', mtInformation,
      [mbok], 0);
    Abort;
  end;

  if ((DM.Q_Referencia.Active = false) or (DM.Q_Referencia.RecordCount = 0))
  then
  begin
    MessageDlg('Nenhum registro a ser alterado!', mtConfirmation, [mbok], 0);
    Abort;
  end;

  if not(DM.Q_Referencia.state in dsEditModes) then
  begin
    DM.Q_Referencia.Edit;
    DBDESCRICAO.SetFocus;
  end;

  tools.HabilitaComponentes(true, 75);
  tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir,
    SpLocalizar, SpImprimir], [false, false, true, true, false, false, false]);
  Label_Status.Caption := 'Alterando registro...';
end;

procedure TFrmPrincipal.ActGravarExecute(Sender: TObject);
var
  v_status: String;
begin
  if not(SpGravar.Enabled) then
    exit;

  if ((Conexao.Inclusao <> 1) and (Conexao.Alteracao <> 1)) then
  begin
    MessageDlg('Voc� n�o tem permiss�o para gravar!', mtInformation, [mbok], 0);
    Abort;
  end;

  if (DM.Q_Referencia.state = dsEdit) then
    v_status := 'E'
  else
    v_status := 'I';

  try
    if DM.Q_Referencia.state in dsEditModes then
      DM.Q_Referencia.Post;
    DM.OraBanco.ApplyUpdates([DM.Q_Referencia], false);

    if (v_status = 'E') then
    begin
      GravaReferenciaProduto(DM.Q_ReferenciaID_REFERENCIA.Value, v_status);
      DM.OraBanco.ApplyUpdates([DM.Q_Integra_Ref_Produto], false);
    end
    else
    begin
      GravaReferenciaNivel(DM.Q_ReferenciaID_REFERENCIA.Value);
      DM.OraBanco.ApplyUpdates([DM.Q_Referencia_Nivel], false);
      GravaReferenciaProduto(DM.Q_ReferenciaID_REFERENCIA.Value, v_status);
      DM.OraBanco.ApplyUpdates([DM.Q_Integra_Ref_Produto], false);
    end;
    DM.OraBanco.Commit;
  except
    on e: Exception do
    begin
      DM.OraBanco.rollback;
      raise Exception.Create('Falha ao gravar registro: ' + e.Message);
    end;
  end;

  tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir,
    SpLocalizar, SpImprimir], [true, true, false, false, true, true, true]);
  tools.HabilitaComponentes(false, 75);
  Label_Status.Caption := '';
  ActCancelarExecute(sender);
  MessageDlg('Registro gravado com sucesso!', mtInformation, [mbok], 0);
end;

procedure TFrmPrincipal.ActCancelarExecute(Sender: TObject);
begin
  if not(SpCancelar.Enabled) then
    exit;

  DM.Q_Referencia.Cancel;
  if (DM.Q_Referencia.RecordCount = 0) then
    ExcluiCampos;

  tools.HabilitaComponentes(false, 75);
  tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir,
    SpLocalizar, SpImprimir], [true, true, false, false, true, true, true]);
  Label_Status.Caption := '';
end;

procedure TFrmPrincipal.ActExcluirExecute(Sender: TObject);
begin
  if not(SpExcluir.Enabled) then
    exit;

  if (Conexao.exclusao <> 1) then
  begin
    MessageDlg('Voc� n�o tem permiss�o para excluir!', mtInformation,
      [mbok], 0);
    Abort;
  end;

  if ((DM.Q_Referencia.Active = false) or (DM.Q_Referencia.RecordCount = 0)) then
  begin
    MessageDlg('Nenhum registro a ser exclu�do!', mtConfirmation, [mbok], 0);
    Abort;
  end;

  if MessageDlg('Tem certeza que deseja excluir esse registro?', mtConfirmation,
    [mbYes, mbNo], 0) = mrNo then
    Abort;

  try
    DM.Q_Referencia.Delete;
    DM.OraBanco.ApplyUpdates([DM.Q_Referencia], false);
    DM.OraBanco.Commit;
  except
    on e: Exception do
    begin
      DM.OraBanco.rollback;
      raise Exception.Create('Falha ao excluir registro: ' + e.Message);
    end;
  end;

  tools.HabilitaComponentes(false, 75);
  tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir,
    SpLocalizar, SpImprimir], [true, true, false, false, true, true, true]);
  Label_Status.Caption := '';
  MessageDlg('Registro exclu�do com sucesso!', mtInformation, [mbok], 0);
end;

procedure TFrmPrincipal.ActLocalizarExecute(Sender: TObject);
begin
  if not(SpLocalizar.Enabled) then
    exit;

  if DM.Q_Referencia.state in dsEditModes then
  begin
    MessageDlg('Finalize a opera��o atual antes de consultar outros registros!',
      mtInformation, [mbok], 0);
    Abort;
  end;

  if PageControl1.ActivePage = Tab_Cadastro then
  begin
    PageControl1.ActivePage := Tab_Consulta;
    FrmPrincipal.WindowState := wsMaximized;
    tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir,
      SpLocalizar, SpImprimir], [false, false, false, false, true, true, true]);
    Label_Status.Caption := 'Consultando registros...';
  end
  else
  begin
    PageControl1.ActivePage := Tab_Cadastro;
    FrmPrincipal.WindowState := wsNormal;
    DM.Q_Referencia.Close;
    DM.Q_Referencia.SetVariable('p_referencia',
    DM.Q_Cons_ReferenciaID_REFERENCIA.Value);
    DM.Q_Referencia.Open;

    // ALIMENTA O CAMPO POSSUI GRADE
    if DM.Q_ReferenciaID_GRADE.Value = null then
      RgPossuiGrade.ItemIndex := 1
    else
      RgPossuiGrade.ItemIndex := 0;

    ExcluiCampos();
    DM.Q_Busca_Nivel.Close;
    DM.Q_Busca_Nivel.SetVariable('p_id_tipo_produto', DM.Q_ReferenciaID_TIPO_PRODUTO.Value);
    DM.Q_Busca_Nivel.Open;

    //AlimentaCampos();
    CriaCampos();
    AlimentaCampos();
    DBTIPO_PRODUTO.Enabled := false;

    AlimentaCamposNivel();

    tools.HabilitaBotoes([SpNovo, SpEditar, SpGravar, SpCancelar, SpExcluir, SpLocalizar, SpImprimir], [true, true, false, false, true, true, true]);
    Label_Status.Caption := '';
  end;
end;

procedure TFrmPrincipal.ActImprimirExecute(Sender: TObject);
begin
  if not(SpImprimir.Enabled) then
    exit;

  // if Q_Cons_Referencia.active = false then

  // if (Q_Cons_Referencia.RecordCount = 0) then
  // begin
  // MessageDlg('Nenhum registro encontrado!',mtConfirmation,[mbOk],0);
  // Abort;
  // end;

  if (Conexao.Relatorio <> 1) then
  begin
    MessageDlg('Voc� n�o tem permiss�o para relat�rio!', mtInformation,
      [mbok], 0);
    Abort;
  end;

  try
    FRM_Impressao := TFRM_Impressao.Create(self);
    FRM_Impressao.m_relatorio := Conexao.Relatorio;
    FRM_Impressao.m_email := Conexao.Email;
    FRM_Impressao.m_pdf := Conexao.Pdf;
    FRM_Impressao.m_txt := Conexao.Txt;
    FRM_Impressao.m_word := Conexao.Word;
    FRM_Impressao.m_html := Conexao.Html;
    FRM_Impressao.m_excel := Conexao.Excel;
    FRM_Impressao.m_diretorio := Conexao.Diretorio_sistema;
    // FRM_Impressao.ppReportImpressao := ppReport_Cons_Referencia;
    FRM_Impressao.ShowModal;
  finally
    FRM_Impressao.Close;
  end;
end;

procedure TFrmPrincipal.ActAjudaExecute(Sender: TObject);
begin
  FrmSobre := TFrmSobre.Create(self);
  FrmSobre.m_acesso := Conexao.Acesso;
  FrmSobre.m_inclusao := Conexao.Inclusao;
  FrmSobre.m_alteracao := Conexao.Alteracao;
  FrmSobre.m_exclusao := Conexao.exclusao;
  FrmSobre.m_relatorio := Conexao.Relatorio;
  FrmSobre.m_email := Conexao.Email;
  FrmSobre.m_pdf := Conexao.Pdf;
  FrmSobre.m_Texto := Conexao.Txt;
  FrmSobre.m_html := Conexao.Html;
  FrmSobre.m_word := Conexao.Word;
  FrmSobre.m_excel := Conexao.Excel;
  FrmSobre.Lbl_Nome_Empresa.Caption := Conexao.Nome_Empresa_Cliente;
  FrmSobre.Lbl_Nome_Software.Caption := Conexao.Nome_Software;
  FrmSobre.Lbl_Usuario.Caption := Conexao.User;
  FrmSobre.Lbl_Banco.Caption := Conexao.Database;
  FrmSobre.m_diretorio := Conexao.Diretorio_sistema;
  FrmSobre.ShowModal;
  FreeAndNil(FrmSobre);
end;

procedure TFrmPrincipal.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Conexao);
end;

procedure TFrmPrincipal.FormKeyPress(Sender: TObject; var Key: Char);
begin
  // escreve somente em mai�sculas
  Key := AnsiUpperCase(Key)[Length(Key)];

  // mudar o foco quando apertar [enter]
  if Key = #13 then
  begin
    if not((ActiveControl is TDBGrid) and (cxGrid_ConsultaDBTableView1.Focused)
      and (ActiveControl is TMemo)) then
    begin
      Key := #0;
      Perform(WM_NEXTDLGCTL, 0, 0);
    end
    else if (ActiveControl is TDBGrid) then
    begin
      if TDBGrid(ActiveControl).selectedindex <
        (TDBGrid(ActiveControl).fieldcount - 1) then
        TDBGrid(ActiveControl).selectedindex := TDBGrid(ActiveControl)
          .selectedindex + 1
      else
        TDBGrid(ActiveControl).selectedindex := 0;
    end;
  end;
end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
  Panel_Titulo.Caption := '          ' + Conexao.Nome_Programa;
  FrmPrincipal.Caption := Conexao.Barra_Titulo;
  Image_Empresa.Picture.LoadFromFile(Conexao.Diretorio_sistema +
    'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(Conexao.Diretorio_sistema +
    'Imagens\logo_prime.ico');
  // DBTIPO_PRODUTO.SetFocus;
end;

procedure TFrmPrincipal.Q_ReferenciaCalcFields(DataSet: TDataSet);
begin
  // Q_Tipo_Produto.close;
  // Q_Tipo_Produto.SetVariable('p_id_tipo_produto',Q_ReferenciaID_TIPO_PRODUTO.Value);
  // Q_Tipo_Produto.Open;
  // Q_ReferenciaTipo_Produto_Descricao.Value := Q_Tipo_ProdutoDESCRICAO.Value;
  //
  // Q_Forma.close;
  // Q_Forma.SetVariable('p_id_forma',Q_ReferenciaID_FORMA.Value);
  // Q_Forma.Open;
  // Q_ReferenciaForma_Descricao.Value := Q_FormaDESCRICAO.Value;
  //
  // Q_Tipo_Material.close;
  // Q_Tipo_Material.SetVariable('p_id_tipo_material',Q_ReferenciaID_TIPO_MATERIAL.Value);
  // Q_Tipo_Material.Open;
  // Q_ReferenciaTipo_Material_Descricao.Value := Q_Tipo_MaterialDESCRICAO.Value;
  //
  // Q_Tipo_Solado.close;
  // Q_Tipo_Solado.SetVariable('p_id_tipo_solado',Q_ReferenciaID_TIPO_SOLADO.Value);
  // Q_Tipo_Solado.Open;
  // Q_ReferenciaTipo_Solado_Descricao.Value := Q_Tipo_SoladoDESCRICAO.Value;
  //
  // Q_Tipo_Corte.close;
  // Q_Tipo_Corte.SetVariable('p_id_tipo_corte',Q_ReferenciaID_TIPO_CORTE.Value);
  // Q_Tipo_Corte.Open;
  // Q_ReferenciaTipo_Corte_Descricao.Value := Q_Tipo_CorteDESCRICAO.Value;
  //
  // Q_Colecao.close;
  // Q_Colecao.SetVariable('p_id_colecao',Q_ReferenciaID_COLECAO.Value);
  // Q_Colecao.Open;
  // Q_ReferenciaDescricao_Colecao.Value := Q_ColecaoDESCRICAO.Value;
  //
  // Q_Evento.close;
  // Q_Evento.SetVariable('p_id_evento',Q_ReferenciaID_EVENTO.Value);
  // Q_Evento.Open;
  // Q_ReferenciaDescricao_Evento.Value := Q_EventoDESCRICAO.Value;
  //
  // Q_Designer.close;
  // Q_Designer.SetVariable('p_id_designer',Q_ReferenciaID_DESIGNER.Value);
  // Q_Designer.Open;
  // Q_ReferenciaNome_Designer.Value := Q_DesignerNOME.Value;
  //
  // Q_Modelista.close;
  // Q_Modelista.SetVariable('p_id_modelista',Q_ReferenciaID_MODELISTA.Value);
  // Q_Modelista.Open;
  // Q_ReferenciaNome_Modelista.Value := Q_ModelistaNOME.Value;
  //
  // if Frm_Vizualizar_Grade <> nil then
  // begin
  //
  // Q_Modelista.close;
  // Q_Modelista.SetVariable('p_id_modelista',Q_ReferenciaID_MODELISTA.Value);
  // Q_Modelista.Open;
  // Q_ReferenciaNome_Modelista.Value := Q_ModelistaNOME.Value;
  //
  // end;

end;

procedure TFrmPrincipal.RgPossuiGradeExit(Sender: TObject);
begin
  Checar_Status_DataSet;

  if RgPossuiGrade.ItemIndex = 0 then
  begin
    if Frm_Vizualizar_Grade = nil then
      Frm_Vizualizar_Grade := TFrm_Vizualizar_Grade.Create(self);

    Frm_Vizualizar_Grade.ShowModal;
  end;

end;

procedure TFrmPrincipal.ExecutaOnExit(speed: TObject);
var
  edit_nivel_nome: String;
  edit_nivel_valor: String;
  edit_desc_nivel_conteudo: String;
begin

  edit_nivel_nome := copy(TEdit(speed).Name, 4, Length(TEdit(speed).Name));
  edit_nivel_valor := TEdit(speed).Text;
  edit_desc_nivel_conteudo := 'ed_desc_' + edit_nivel_nome;

  if edit_nivel_valor <> EmptyStr then
  begin
    if DM.Q_Busca_Nivel.RecordCount > 0 then
    begin
      DM.Q_Busca_Nivel.First;
      while not DM.Q_Busca_Nivel.Eof do
      begin
        if DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger = StrToInt(edit_nivel_nome)
        then
        begin
          DM.Q_Busca_Nivel_Conteudo.Close;
          DM.Q_Busca_Nivel_Conteudo.SetVariable('P_ID_NIVEL_PRODUTO',
            StrToInt(edit_nivel_nome));
          DM.Q_Busca_Nivel_Conteudo.SetVariable('P_CONTEUDO', edit_nivel_valor);
          DM.Q_Busca_Nivel_Conteudo.Open;

          if DM.Q_Busca_Nivel_Conteudo.RecordCount = 0 then
          begin
            TEdit(speed).Text := '';
            TEdit(FindComponent(edit_desc_nivel_conteudo)).Text := '';
            TEdit(speed).SetFocus;
            MessageDlg('N�o cadastrado!', mtInformation, [mbok], 0);
            Abort;
          end
          else
          begin
            TEdit(FindComponent(edit_desc_nivel_conteudo)).Text :=
              DM.Q_Busca_Nivel_ConteudoDESCRICAO.Value;
            GeraIdReferencia;
          end;
        end;

        DM.Q_Busca_Nivel.next;
      end;
    end;
  end;
end;

procedure TFrmPrincipal.ExcluiCampos;
var
  obj_label: String;
  obj_edit_id: String;
  obj_edit_desc: String;
  obj_sp: String;

begin
  if DM.Q_Busca_Nivel.Active then
  begin

    DM.Q_Busca_Nivel.First;
    while not DM.Q_Busca_Nivel.Eof do
    begin
      obj_label := 'lb_';
      obj_edit_id := 'ed_';
      obj_edit_desc := 'ed_desc_';
      obj_sp := 'sp_';

      // EXCLUINDO OS LABELS
      obj_label := obj_label + inttostr(DM.Q_Busca_Nivel.RecNo);
      TLabel(FindComponent(obj_label)).Free;

      // EXCLUINDO O EDIT DO CODIGO
      obj_edit_id := obj_edit_id +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TEdit(FindComponent(obj_edit_id)).Free;

      // EXCLUINDO O EDIT DA DESCRICAO
      obj_edit_desc := obj_edit_desc +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TEdit(FindComponent(obj_edit_desc)).Free;

      // EXCLUINDO OS SPEED BUTTONS
      obj_sp := obj_sp + inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TSpeedButton(FindComponent(obj_sp)).Free;

      DM.Q_Busca_Nivel.next;
    end;
  end;
end;

procedure TFrmPrincipal.DesabilitaCampos;
var
  obj_label: String;
  obj_edit_id: String;
  obj_edit_desc: String;
  obj_sp: String;

begin
  if DM.Q_Busca_Nivel.Active then
  begin

    DM.Q_Busca_Nivel.First;
    while not DM.Q_Busca_Nivel.Eof do
    begin
      // obj_label     := 'lb_';
      obj_edit_id := 'ed_';
      obj_edit_desc := 'ed_desc_';
      obj_sp := 'sp_';

      // EXCLUINDO OS LABELS
      // obj_label     :=  obj_label   + inttostr(Q_Busca_Nivel.RecNo);
      // TLabel( FindComponent( obj_label )).Free;

      // EXCLUINDO O EDIT DO CODIGO
      obj_edit_id := obj_edit_id +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TEdit(FindComponent(obj_edit_id)).Enabled := false;

      // EXCLUINDO O EDIT DA DESCRICAO
      obj_edit_desc := obj_edit_desc +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TEdit(FindComponent(obj_edit_desc)).Enabled := false;

      // EXCLUINDO OS SPEED BUTTONS
      obj_sp := obj_sp + inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TSpeedButton(FindComponent(obj_sp)).Enabled := false;

      DM.Q_Busca_Nivel.next;
    end;
  end;
end;

procedure TFrmPrincipal.GravaReferenciaNivel(referencia: String);
var
  obj_edit_id: String;
  obj_edit_id_value: String;
begin
  if DM.Q_Busca_Nivel.Active then
  begin

    DM.Q_Busca_Nivel.First;
    while not DM.Q_Busca_Nivel.Eof do
    begin

      obj_edit_id := 'ed_';
      obj_edit_id := obj_edit_id +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      obj_edit_id_value := TEdit(FindComponent(obj_edit_id)).Text;

      DM.Q_Busca_Nivel_Conteudo.Close;
      DM.Q_Busca_Nivel_Conteudo.SetVariable('P_ID_NIVEL_PRODUTO',
      DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      DM.Q_Busca_Nivel_Conteudo.SetVariable('P_CONTEUDO', obj_edit_id_value);
      DM.Q_Busca_Nivel_Conteudo.Open;

      // //PEGAO O CODIGO DA SEQUENCE SQ_REFERENCIA_NIVEL
      // Q_Busca_Id_Referencia_Nivel.close;
      // Q_Busca_Id_Referencia_Nivel.open;

      DM.Q_Referencia_Nivel.Append;
      DM.Q_Referencia_NivelID_REFERENCIA_NIVEL.Value :=
      BuscaValorSequence('SQ_REFERENCIA_NIVEL', DM.OraBanco);
      // Q_Busca_Id_Referencia_NivelID_REFERENCIA_NIVEL.Value;
      DM.Q_Referencia_NivelID_REFERENCIA.Value := referencia;
      DM.Q_Referencia_NivelID_NIVEL_CONTEUDO.Value :=
        DM.Q_Busca_Nivel_ConteudoID_NIVEL_CONTEUDO.Value;
      DM.Q_Referencia_Nivel.Post;

      DM.Q_Busca_Nivel.next;
    end;
  end;
end;

procedure TFrmPrincipal.GravaReferenciaProduto(referencia: String;
  status: String);
var
  v_produto_id, i, v_produto_numero_id: Integer;
  v_numero_ativo, v_numero: string;
  v_achou: boolean;

begin

  if status = 'I' then
  begin
    v_produto_id := BuscaValorSequence('SQ_PRODUTO', DM.OraBanco);

    // GRAVA NA PRODUTO
    DM.Q_Integra_Ref_Produto.Append;
    DM.Q_Integra_Ref_ProdutoID_PRODUTO.Value := v_produto_id;
    DM.Q_Integra_Ref_ProdutoCODIGO.Value := referencia;
    DM.Q_Integra_Ref_ProdutoDESCRICAO.Value := DM.Q_ReferenciaDESCRICAO.Value;
    DM.Q_Integra_Ref_ProdutoID_UNIDADE_PRODUCAO.Value := 'UN';
    DM.Q_Integra_Ref_ProdutoID_UNIDADE_VENDA.Value := 'UN';
    DM.Q_Integra_Ref_ProdutoID_SUB_GRUPO.AsInteger := 5407;
    DM.Q_Integra_Ref_ProdutoID_GRADE.Value := DM.Q_ReferenciaID_GRADE.Value;
    DM.Q_Integra_Ref_ProdutoATIVO.Value := 'S';
    DM.Q_Integra_Ref_Produto.Post;

    for i := 1 to 25 do
    begin
      v_numero_ativo := TStringField(DM.FindComponent('MemoryTableSTATUSNUMERACAO' + inttostr(i))).AsString;
      v_numero := TStringField(DM.FindComponent('MemoryTableNUMERACAO' + inttostr(i))).AsString;

      if v_numero <> '' then
      begin
        DM.Q_Integra_Ref_Produto_Numero.Append;
        DM.Q_Integra_Ref_Produto_NumeroID_PRODUTO.Value := v_produto_id;
        DM.Q_Integra_Ref_Produto_NumeroPOSICAO.Value := i;
        DM.Q_Integra_Ref_Produto_NumeroNUMERO.Value := v_numero;
        DM.Q_Integra_Ref_Produto_NumeroATIVO.Value := v_numero_ativo;
        DM.Q_Integra_Ref_Produto_Numero.Post;

        DM.OraBanco.ApplyUpdates([DM.Q_Integra_Ref_Produto_Numero], false);
      end;
    end;

  end
  else
  begin
    v_produto_numero_id := 0;
    for i := 1 to 25 do
    begin
      v_numero_ativo :=
        TStringField(DM.FindComponent('MemoryTableSTATUSNUMERACAO' + inttostr(i))
        ).AsString;

      v_achou := DM.Q_Referencia_Produto_Numero.Locate('POSICAO', i, [loPartialKey]);

      if v_achou then
      begin
        v_produto_numero_id :=
          DM.Q_Referencia_Produto_NumeroID_PRODUTO_NUMERO.AsInteger;
        // v_produto_id := Q_Referencia_Produto_NumeroID_PRODUTO.Asinteger;
      end;

      if v_produto_numero_id <> 0 then
      begin
        DM.Q_Integra_Ref_Produto_Numero.Close;
        DM.Q_Integra_Ref_Produto_Numero.SetVariable('P_ID_PRODUTO_NUMERO',
          v_produto_numero_id);
        DM.Q_Integra_Ref_Produto_Numero.Open;

        DM.Q_Integra_Ref_Produto_Numero.Edit;
        DM.Q_Integra_Ref_Produto_NumeroID_PRODUTO_NUMERO.Value :=
          v_produto_numero_id;
        // Q_Integra_Ref_Produto_NumeroID_PRODUTO.Value := v_produto_id;
        // Q_Integra_Ref_Produto_NumeroPOSICAO.Value := i;
        // Q_Integra_Ref_Produto_NumeroNUMERO.Value := v_numero;
        DM.Q_Integra_Ref_Produto_NumeroATIVO.Value := v_numero_ativo;
        DM.Q_Integra_Ref_Produto_Numero.Post;
      end;
    end;
  end;
end;

procedure TFrmPrincipal.AlimentaCamposNivel();
var
  obj_edit_id: String;
  obj_edit_id_value: String;
  obj_edit_desc: String;
  obj_sp: String;
begin
  if DM.Q_Busca_Nivel.Active then
  begin

    DM.Q_Busca_Nivel.First;
    while not DM.Q_Busca_Nivel.Eof do
    begin

      obj_edit_id := 'ed_';
      obj_edit_id := obj_edit_id +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      obj_edit_desc := 'ed_desc_';
      obj_edit_desc := obj_edit_desc +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      obj_sp := 'sp_';
      obj_sp := obj_sp + inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      TSpeedButton(FindComponent(obj_sp)).Enabled := false;

      DM.Q_Cons_Referencia_Nivel.Close;
      DM.Q_Cons_Referencia_Nivel.SetVariable('p_id_referencia',
        DM.Q_ReferenciaID_REFERENCIA.Value);
      DM.Q_Cons_Referencia_Nivel.SetVariable('p_id_nivel_produto',
        DM.Q_Busca_NivelID_NIVEL_PRODUTO.Value);
      DM.Q_Cons_Referencia_Nivel.Open;

      TEdit(FindComponent(obj_edit_id)).Text :=
        DM.Q_Cons_Referencia_NivelCONTEUDO.Value;
      TEdit(FindComponent(obj_edit_id)).Enabled := false;
      TEdit(FindComponent(obj_edit_desc)).Text :=
        DM.Q_Cons_Referencia_NivelDESCRICAO.Value;
      TEdit(FindComponent(obj_edit_desc)).Enabled := false;

      DM.Q_Busca_Nivel.next;
    end;
  end;
end;

procedure TFrmPrincipal.AlimentaCampos;
var
  obj_edit_id: String;
  obj_edit_id_value: String;
  obj_edit_desc: String;
  obj_sp: String;
begin
   DBTIPO_PRODUTOExit(self);
   DBTIPO_SOLADOExit(self);
   DBFORMAExit(self);
   DBTIPO_CORTEExit(self);
   DBCOLECAO_LANCAMENTOExit(self);
   DBEVENTO_LANCAMENTOExit(self);
   DBCODIGO_DESIGNERExit(self);
   DBCODIGO_MODELISTAExit(self);
   DBTIPO_MATERIALExit(self);
   ed_clienteExit(self);
end;

procedure TFrmPrincipal.CriaCampos;
var
  itemAux: Integer;
begin

  if DM.Q_Busca_Nivel.Active then
  begin
    DM.Q_Busca_Nivel.First;
    itemAux := 1;
    while not DM.Q_Busca_Nivel.Eof do
    begin

      if (DM.Q_Busca_Nivel.RecNo = 5) then
        itemAux := 1;

      with TLabel.Create(self) do
      begin
        Parent := ScrollBox_Niveis;
        Name := 'lb_' + inttostr(DM.Q_Busca_Nivel.RecNo);

        if (DM.Q_Busca_Nivel.RecNo >= 5) and (DM.Q_Busca_Nivel.RecNo <= 8) then
        begin
          Top := -35 + (itemAux * 42);
          Left := 315
        end
        else
        begin
          Top := -35 + (itemAux * 42);
          Left := 5;
        end;
        font.Size := 8;
        Caption := DM.Q_Busca_NivelDESCRICAO.Value;
        font.Name := 'Arial';
      end;

      with TEdit.Create(self) do
      begin
        Parent := ScrollBox_Niveis;

        if (DM.Q_Busca_Nivel.RecNo >= 5) and (DM.Q_Busca_Nivel.RecNo <= 8) then
        begin
          Top := -20 + (itemAux * 42);
          Left := 315;
        end
        else
        begin
          Top := -20 + (itemAux * 42);
          Left := 5;
        end;
        Width := 58;
        Name := 'ed_' + inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
        Text := '';
        BevelInner := bvLowered;
        BevelKind := bkSoft;
        BevelOuter := bvSpace;
        BorderStyle := bsNone;
        OnExit := ExecutaOnExit;
        OnDblClick := AbreConsultaNivelConteudo;
        OnKeyDown := ExecutaOnKeyDown;
      end;

      with TSpeedButton.Create(self) do
      begin
        Parent := ScrollBox_Niveis;
        Height := 21;
        Width := 23;
        Name := 'sp_' + inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
        if (DM.Q_Busca_Nivel.RecNo >= 5) and (DM.Q_Busca_Nivel.RecNo <= 8) then
        begin
          Left := 377;
          Top := -20 + (itemAux * 42);
        end
        else
        begin
          Left := 67;
          Top := -20 + (itemAux * 42);
        end;
        Glyph := Spd_Tipo_Produto.Glyph;
        Flat := true;
        OnClick := AbreConsultaNivelConteudo;
      end;

      with TEdit.Create(self) do
      begin
        Parent := ScrollBox_Niveis;
        if (DM.Q_Busca_Nivel.RecNo >= 5) and (DM.Q_Busca_Nivel.RecNo <= 8) then
        begin
          Top := -20 + (itemAux * 42);
          Left := 405;
        end
        else
        begin
          Top := -20 + (itemAux * 42);
          Left := 95;
        end;

        Width := 210;
        Name := 'ed_desc_' + inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
        Text := '';
        BevelInner := bvLowered;
        BevelKind := bkSoft;
        BevelOuter := bvSpace;
        BorderStyle := bsNone;
        Enabled := false;
        Color := clBtnFace;
      end;

      itemAux := itemAux + 1;
      DM.Q_Busca_Nivel.next;
    end;
  end;
end;

procedure TFrmPrincipal.DBTIPO_PRODUTOExit(Sender: TObject);
begin

  if DBTIPO_PRODUTO.Text <> '' then
    ValidaChave('TIPO_PRODUTO', 'S', 'DBEDIT', DBTIPO_PRODUTO.Name, nil, nil, nil,
      nil, DM.OraBanco, FrmPrincipal);

  // CRIA CAMPOS DE NIVEIS
  if not DM.Q_Busca_Nivel.Active then
  begin
    DM.Q_Busca_Nivel.Close;
    DM.Q_Busca_Nivel.SetVariable('p_id_tipo_produto',
    DM.Q_ReferenciaID_TIPO_PRODUTO.Value);
    DM.Q_Busca_Nivel.Open;
    CriaCampos;
  end
  else
  begin
    ExcluiCampos;
    DM.Q_Busca_Nivel.Close;
    DM.Q_Busca_Nivel.SetVariable('p_id_tipo_produto',
    DM.Q_ReferenciaID_TIPO_PRODUTO.Value);
    DM.Q_Busca_Nivel.Open;
    CriaCampos;
  end;
end;

procedure TFrmPrincipal.GeraIdReferencia;
var
  id_referencia: String;
  obj_edit_id: String;
begin
  if DM.Q_Busca_Nivel.Active then
  begin
    id_referencia := '';
    DM.Q_Busca_Nivel.First;
    while not DM.Q_Busca_Nivel.Eof do
    begin
      obj_edit_id := 'ed_';
      // EXCLUINDO O EDIT DO CODIGO
      obj_edit_id := obj_edit_id +
        inttostr(DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger);
      id_referencia := id_referencia + TEdit(FindComponent(obj_edit_id)).Text;

      DM.Q_Busca_Nivel.next;
    end;
    DM.Q_ReferenciaID_REFERENCIA.AsString := id_referencia;
  end;
end;

procedure TFrmPrincipal.DBCORDblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Cor := TFrm_Cons_Cor.Create(self);
  Frm_Cons_Cor.P_Sessao := DM.OraBanco;
  Frm_Cons_Cor.p_user := Conexao.User;
  Frm_Cons_Cor.p_password := Conexao.Password;
  Frm_Cons_Cor.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Cor.ShowModal = mrOk then
    DM.Q_ReferenciaID_COR.Value := Frm_Cons_Cor.Q_Cons_CorID_COR.Value;
end;

procedure TFrmPrincipal.DBCORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.AbreConsultaNivelConteudo(speed: TObject);
var
  id_nivel_produto: String;
  edit_nivel_valor: String;
  edit_desc_nivel_conteudo: String;
begin

  id_nivel_produto := copy(TSpeedButton(speed).Name, 4,
    Length(TSpeedButton(speed).Name));
  edit_nivel_valor := 'ed_' + id_nivel_produto;
  edit_desc_nivel_conteudo := 'ed_desc_' + id_nivel_produto;

  if id_nivel_produto <> EmptyStr then
  begin
    if DM.Q_Busca_Nivel.RecordCount > 0 then
    begin
      DM.Q_Busca_Nivel.First;
      while not DM.Q_Busca_Nivel.Eof do
      begin
        if DM.Q_Busca_NivelID_NIVEL_PRODUTO.AsInteger = StrToInt(id_nivel_produto)
        then
        begin
          if not(DM.Q_Referencia.state in dsEditModes) then
          begin
            MessageDlg('Abra um registro antes!', mtInformation, [mbok], 0);
            Abort;
          end;

          try
            Frm_Cons_Nivel_Conteudo := TFrm_Cons_Nivel_Conteudo.Create(self);
            Frm_Cons_Nivel_Conteudo.P_Sessao := DM.OraBanco;
            Frm_Cons_Nivel_Conteudo.p_user := Conexao.User;
            Frm_Cons_Nivel_Conteudo.p_password := Conexao.Password;
            Frm_Cons_Nivel_Conteudo.m_diretorio_sistema :=
              Conexao.Diretorio_sistema;
            Frm_Cons_Nivel_Conteudo.m_id_nivel_produto :=
              StrToInt(id_nivel_produto);
            Frm_Cons_Nivel_Conteudo.m_titulo_consulta :=
              DM.Q_Busca_NivelDESCRICAO.AsString;

            if Frm_Cons_Nivel_Conteudo.ShowModal = mrOk then
            begin
              TEdit(FindComponent(edit_nivel_valor)).Text :=
                Frm_Cons_Nivel_Conteudo.Q_Cons_Nivel_ConteudoCONTEUDO.AsString;
              TEdit(FindComponent(edit_desc_nivel_conteudo)).Text :=
                Frm_Cons_Nivel_Conteudo.Q_Cons_Nivel_ConteudoDESCRICAO.AsString;
              GeraIdReferencia;
            end;

          finally
            Frm_Cons_Nivel_Conteudo.Free;
          end;
        end;

        DM.Q_Busca_Nivel.next;
      end;
    end;
  end;
end;

procedure TFrmPrincipal.DBFORMAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.Spd_FormaClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Forma := TFrm_Cons_Forma.Create(self);
  Frm_Cons_Forma.P_Sessao := DM.OraBanco;
  Frm_Cons_Forma.p_user := Conexao.User;
  Frm_Cons_Forma.p_password := Conexao.Password;
  Frm_Cons_Forma.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Forma.ShowModal = mrOk then
    DM.Q_ReferenciaID_FORMA.Value := Frm_Cons_Forma.Q_Cons_FormaID_FORMA.Value;
end;

procedure TFrmPrincipal.DBFORMADblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Forma := TFrm_Cons_Forma.Create(self);
  Frm_Cons_Forma.P_Sessao := DM.OraBanco;
  Frm_Cons_Forma.p_user := Conexao.User;
  Frm_Cons_Forma.p_password := Conexao.Password;
  Frm_Cons_Forma.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Forma.ShowModal = mrOk then
    begin
      DBFORMA.Text := Frm_Cons_Forma.Q_Cons_FormaID_FORMA.Value;
      DM.Q_ReferenciaID_FORMA.Value := ValidaChave('FORMA', 'S', 'DBEDIT', DBFORMA.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal);
    end;
end;

procedure TFrmPrincipal.DBFORMAExit(Sender: TObject);
begin
  if DBFORMA.Text <> '' then
    ValidaChave('FORMA', 'S', 'DBEDIT', DBFORMA.Name, nil, nil, nil, nil,
      DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBTIPO_MATERIALKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBTIPO_MATERIALDblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Tipo_Material := TFrm_Cons_Tipo_Material.Create(self);
  Frm_Cons_Tipo_Material.P_Sessao := DM.OraBanco;
  Frm_Cons_Tipo_Material.p_user := Conexao.User;
  Frm_Cons_Tipo_Material.p_password := Conexao.Password;
  Frm_Cons_Tipo_Material.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Tipo_Material.ShowModal = mrOk then
    begin
      DBTIPO_MATERIAL.Text := Frm_Cons_Tipo_Material.Q_Cons_Tipo_MaterialID_TIPO_MATERIAL.AsString;
      DM.Q_ReferenciaID_TIPO_MATERIAL.AsInteger := StrToInt(ValidaChave('TIPO_MATERIAL', 'S', 'DBEDIT', DBTIPO_MATERIAL.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal));
    end;
end;

procedure TFrmPrincipal.DBTIPO_MATERIALExit(Sender: TObject);
begin
  if DBTIPO_MATERIAL.Text <> '' then
    ValidaChave('TIPO_MATERIAL', 'S', 'DBEDIT', DBTIPO_MATERIAL.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBTIPO_SOLADODblClick(Sender: TObject);
begin
  Checar_Status_DataSet;
  Frm_Cons_Tipo_Solado := TFrm_Cons_Tipo_Solado.Create(self);
  Frm_Cons_Tipo_Solado.P_Sessao := DM.OraBanco;
  Frm_Cons_Tipo_Solado.p_user := Conexao.User;
  Frm_Cons_Tipo_Solado.p_password := Conexao.Password;
  Frm_Cons_Tipo_Solado.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Tipo_Solado.ShowModal = mrOk then
  begin
    DBTIPO_SOLADO.Text :=  IntToStr(Frm_Cons_Tipo_Solado.Q_Cons_Tipo_SoladoID_TIPO_SOLADO.AsInteger);
    DM.Q_ReferenciaID_TIPO_SOLADO.Value := StrToInt(ValidaChave('TIPO_SOLADO', 'S', 'DBEDIT', DBTIPO_SOLADO.Name, nil, nil, nil,
      nil, DM.OraBanco, FrmPrincipal));
  end;
end;

procedure TFrmPrincipal.DBTIPO_SOLADOExit(Sender: TObject);
begin
  if DBTIPO_SOLADO.Text <> '' then
    ValidaChave('TIPO_SOLADO', 'S', 'DBEDIT', DBTIPO_SOLADO.Name, nil, nil, nil,
      nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBTIPO_SOLADOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBTIPO_CORTEDblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Tipo_Corte := TFrm_Cons_Tipo_Corte.Create(self);
  Frm_Cons_Tipo_Corte.P_Sessao := DM.OraBanco;
  Frm_Cons_Tipo_Corte.p_user := Conexao.User;
  Frm_Cons_Tipo_Corte.p_password := Conexao.Password;
  Frm_Cons_Tipo_Corte.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Tipo_Corte.ShowModal = mrOk then
    begin
      DBTIPO_CORTE.text := Frm_Cons_Tipo_Corte.Q_Cons_Tipo_CorteID_TIPO_CORTE.AsString;
      DM.Q_ReferenciaID_TIPO_CORTE.AsInteger := StrToInt(ValidaChave('TIPO_CORTE', 'S', 'DBEDIT', DBTIPO_CORTE.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal));
    end;
end;

procedure TFrmPrincipal.DBTIPO_CORTEExit(Sender: TObject);
begin
  if DBTIPO_CORTE.Text <> '' then
    ValidaChave('TIPO_CORTE', 'S', 'DBEDIT', DBTIPO_CORTE.Name, nil, nil, nil,
      nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBTIPO_CORTEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.Edt_ConsultaChange(Sender: TObject);
begin
  with DM.Q_Cons_Referencia do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      SetVariable('p_id_referencia', Edt_Consulta.Text + '%');
      Open;
    end;
  end;
end;

procedure TFrmPrincipal.DBCOLECAO_LANCAMENTOKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBCOLECAO_LANCAMENTODblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Colecao := TFrm_Cons_Colecao.Create(self);
  Frm_Cons_Colecao.P_Sessao := DM.OraBanco;
  Frm_Cons_Colecao.p_user := Conexao.User;
  Frm_Cons_Colecao.p_password := Conexao.Password;
  Frm_Cons_Colecao.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Colecao.ShowModal = mrOk then
    begin
      DBCOLECAO_LANCAMENTO.Text := Frm_Cons_Colecao.Q_Cons_ColecaoID_COLECAO.AsString;
      DM.Q_ReferenciaID_COLECAO.AsInteger := StrToInt(ValidaChave('COLECAO', 'S', 'DBEDIT', DBCOLECAO_LANCAMENTO.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal));
    end;
end;

procedure TFrmPrincipal.DBCOLECAO_LANCAMENTOExit(Sender: TObject);
begin
  if DBCOLECAO_LANCAMENTO.Text <> '' then
    ValidaChave('COLECAO', 'S', 'DBEDIT', DBCOLECAO_LANCAMENTO.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBEVENTO_LANCAMENTOKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBEVENTO_LANCAMENTODblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Evento := TFrm_Cons_Evento.Create(self);
  Frm_Cons_Evento.P_Sessao := DM.OraBanco;
  Frm_Cons_Evento.p_user := Conexao.User;
  Frm_Cons_Evento.p_password := Conexao.Password;
  Frm_Cons_Evento.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Evento.ShowModal = mrOk then
    begin
      DBEVENTO_LANCAMENTO.Text := Frm_Cons_Evento.Q_Cons_EventoID_EVENTO.AsString;
      DM.Q_ReferenciaID_EVENTO.AsInteger := StrToInt(ValidaChave('EVENTO', 'S', 'DBEDIT', DBEVENTO_LANCAMENTO.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal));
    end;
end;

procedure TFrmPrincipal.DBEVENTO_LANCAMENTOExit(Sender: TObject);
begin
  if DBEVENTO_LANCAMENTO.Text <> '' then
    ValidaChave('EVENTO', 'S', 'DBEDIT', DBEVENTO_LANCAMENTO.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBCODIGO_DESIGNERKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBCODIGO_DESIGNERDblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Designer := TFrm_Cons_Designer.Create(self);
  Frm_Cons_Designer.P_Sessao := DM.OraBanco;
  Frm_Cons_Designer.p_user := Conexao.User;
  Frm_Cons_Designer.p_password := Conexao.Password;
  Frm_Cons_Designer.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Designer.ShowModal = mrOk then
    begin
      DBCODIGO_DESIGNER.Text := Frm_Cons_Designer.Q_Cons_DesignerID_DESIGNER.AsString;
      DM.Q_ReferenciaID_DESIGNER.AsInteger := StrToInt(ValidaChave('DESIGNER', 'S', 'DBEDIT', DBCODIGO_DESIGNER.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal));
    end;
end;

procedure TFrmPrincipal.DBCODIGO_DESIGNERExit(Sender: TObject);
begin
  if DBCODIGO_DESIGNER.Text <> '' then
    ValidaChave('DESIGNER', 'S', 'DBEDIT', DBCODIGO_DESIGNER.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBCODIGO_MODELISTAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBCODIGO_MODELISTADblClick(Sender: TObject);
begin
  Checar_Status_DataSet;

  Frm_Cons_Modelista := TFrm_Cons_Modelista.Create(self);
  Frm_Cons_Modelista.P_Sessao := DM.OraBanco;
  Frm_Cons_Modelista.p_user := Conexao.User;
  Frm_Cons_Modelista.p_password := Conexao.Password;
  Frm_Cons_Modelista.m_diretorio_sistema := Conexao.Diretorio_sistema;

  if Frm_Cons_Modelista.ShowModal = mrOk then
    begin
      DBCODIGO_MODELISTA.Text := Frm_Cons_Modelista.Q_Cons_ModelistaID_MODELISTA.AsString;
      DM.Q_ReferenciaID_MODELISTA.AsInteger := StrToInt(ValidaChave('MODELISTA', 'S', 'DBEDIT', DBCODIGO_MODELISTA.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal));
    end;
end;

procedure TFrmPrincipal.DBCODIGO_MODELISTAExit(Sender: TObject);
begin
  if DBCODIGO_MODELISTA.Text <> '' then
    ValidaChave('MODELISTA', 'S', 'DBEDIT', DBCODIGO_MODELISTA.Name, nil, nil, nil, nil, DM.OraBanco, FrmPrincipal);
end;

procedure TFrmPrincipal.DBTIPO_PRODUTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.DBTIPO_PRODUTODblClick(Sender: TObject);
begin
  if not(DM.Q_Referencia.state in dsEditModes) then
  begin
    MessageDlg('Abra um registro antes!', mtInformation, [mbok], 0);
    Abort;
  end;

  try
    Frm_Cons_Tipo_Produto := TFrm_Cons_Tipo_Produto.Create(self);
    Frm_Cons_Tipo_Produto.P_Sessao := DM.OraBanco;
    Frm_Cons_Tipo_Produto.p_user := Conexao.User;
    Frm_Cons_Tipo_Produto.p_password := Conexao.Password;
    Frm_Cons_Tipo_Produto.m_diretorio_sistema := Conexao.Diretorio_sistema;

    if Frm_Cons_Tipo_Produto.ShowModal = mrOk then
    begin
       DBTIPO_PRODUTO.Text :=
        IntToStr(Frm_Cons_Tipo_Produto.Q_Cons_Tipo_ProdutoID_TIPO_PRODUTO.AsInteger);
       DM.Q_ReferenciaID_TIPO_PRODUTO.AsInteger := StrToInt(ValidaChave('TIPO_PRODUTO', 'S', 'DBEDIT', DBTIPO_PRODUTO.Name, nil, nil, nil,
        nil, DM.OraBanco, FrmPrincipal));
    end;
  finally
    Frm_Cons_Tipo_Produto.Free;
  end;
end;

procedure TFrmPrincipal.ExecutaOnKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TEdit).OnDblClick(Sender);
end;

procedure TFrmPrincipal.Spd_ClienteClick(Sender: TObject);
var
  tipos: TStringList;
begin
  try
    Frm_Cons_Empresa := TFrm_Cons_Empresa.Create(self);
    Frm_Cons_Empresa.P_Sessao := DM.OraBanco;
    Frm_Cons_Empresa.p_user := Conexao.User;
    Frm_Cons_Empresa.p_password := Conexao.Password;
    Frm_Cons_Empresa.m_diretorio_sistema := Conexao.Diretorio_sistema;
    Frm_Cons_Empresa.P_Tipo_Empresa := '2';
    Frm_Cons_Empresa.P_Campo_Default := 'FANTASIA';
    if Frm_Cons_Empresa.ShowModal = mrOk then
    begin
      tipos := TStringList.Create;
      tipos.add('2');
      ed_cliente.Text := Frm_Cons_Empresa.Q_Cons_EmpresaCNPJ_CPF.Value;
      v_id_empresa := ValidaEmpresa('EDIT', ed_cliente, nil, nil, nil, nil, nil,
        DM.OraBanco, FrmPrincipal);
      DBOBSERVACAO.SetFocus;
    end;
  finally
    Frm_Cons_Empresa.Free;
  end;
end;

procedure TFrmPrincipal.ed_clienteExit(Sender: TObject);
var
  tipos: TStringList;
begin
  if ed_cliente.Text <> '' then
  begin
    tipos := TStringList.Create;
    tipos.add('2');
    tipos.add('3');
    v_id_empresa := ValidaEmpresa('EDIT', ed_cliente, nil, nil, nil, nil, nil,
      DM.OraBanco, FrmPrincipal);
  end;
end;

procedure TFrmPrincipal.ed_clienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_f8 then
    (Sender as TDBEdit).OnDblClick(self);
end;

procedure TFrmPrincipal.Spd_Busca_Foto1Click(Sender: TObject);
begin
  Checar_Status_DataSet;
  if OpenPictureDialog.Execute then
  begin
    try
      Image1.Picture.LoadFromFile(OpenPictureDialog.FileName);
      DM.Q_ReferenciaFOTO1.LoadFromFile(OpenPictureDialog.FileName);
    except
    end;
  end;
end;

procedure TFrmPrincipal.Spd_Limpa_Foto1Click(Sender: TObject);
begin
  Checar_Status_DataSet;
  try
    Image1.Picture := NIL;
    DM.Q_ReferenciaFOTO1.Clear;
  except
  end;
end;

procedure TFrmPrincipal.Spd_Limpa_Foto2Click(Sender: TObject);
begin
  Checar_Status_DataSet;
  try
    Image2.Picture := NIL;
    DM.Q_ReferenciaFOTO2.Clear;
  except
  end;
end;

procedure TFrmPrincipal.Spd_Busca_Foto2Click(Sender: TObject);
begin
  Checar_Status_DataSet;
  if OpenPictureDialog.Execute then
  begin
    try
      Image2.Picture.LoadFromFile(OpenPictureDialog.FileName);
      DM.Q_ReferenciaFOTO2.LoadFromFile(OpenPictureDialog.FileName);
    except
    end;
  end;
end;

procedure TFrmPrincipal.Spd_Limpa_Foto3Click(Sender: TObject);
begin
  Checar_Status_DataSet;
  try
    Image3.Picture := NIL;
    DM.Q_ReferenciaFOTO3.Clear;
  except
  end;
end;

procedure TFrmPrincipal.spVisualizarGradeClick(Sender: TObject);
begin
  Checar_Status_DataSet;
  if Frm_Vizualizar_Grade = nil then
    Frm_Vizualizar_Grade := TFrm_Vizualizar_Grade.Create(self);

  Frm_Vizualizar_Grade.ShowModal;
end;

procedure TFrmPrincipal.Spd_Busca_Foto3Click(Sender: TObject);
begin
  Checar_Status_DataSet;
  if OpenPictureDialog.Execute then
  begin
    try
      Image3.Picture.LoadFromFile(OpenPictureDialog.FileName);
      DM.Q_ReferenciaFOTO3.LoadFromFile(OpenPictureDialog.FileName);
    except
    end;
  end;
end;

procedure TFrmPrincipal.Image1DblClick(Sender: TObject);
begin
  Frm_Vizualizar_Imagem := TFrm_Vizualizar_Imagem.Create(self);
  Frm_Vizualizar_Imagem.m_diretorio_sistema := Conexao.Diretorio_sistema;
  Frm_Vizualizar_Imagem.Image1.Picture.Assign(Image1.Picture);
  Frm_Vizualizar_Imagem.ShowModal;
end;

procedure TFrmPrincipal.Image2DblClick(Sender: TObject);
begin
  Frm_Vizualizar_Imagem := TFrm_Vizualizar_Imagem.Create(self);
  Frm_Vizualizar_Imagem.m_diretorio_sistema := Conexao.Diretorio_sistema;
  Frm_Vizualizar_Imagem.Image1.Picture.Assign(Image2.Picture);
  Frm_Vizualizar_Imagem.ShowModal;
end;

procedure TFrmPrincipal.Image3DblClick(Sender: TObject);
begin
  Frm_Vizualizar_Imagem := TFrm_Vizualizar_Imagem.Create(self);
  Frm_Vizualizar_Imagem.m_diretorio_sistema := Conexao.Diretorio_sistema;
  Frm_Vizualizar_Imagem.Image1.Picture.Assign(Image3.Picture);
  Frm_Vizualizar_Imagem.ShowModal;
end;

end.
