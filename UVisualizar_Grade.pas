unit UVisualizar_Grade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, Vcl.Buttons, Data.DB, OracleData,
  Bde.DBTables, Vcl.Grids, Vcl.DBGrids;

type
  TFrm_Vizualizar_Grade = class(TForm)
    pnCabecalho: TPanel;
    Label20: TLabel;
    Spd_Tipo_Solado: TSpeedButton;
    Label32: TLabel;
    Spd_Colecao: TSpeedButton;
    Label36: TLabel;
    Spd_Modelista: TSpeedButton;
    DBGRADE: TDBEdit;
    DBCOLECAO_LANCAMENTO: TDBEdit;
    DBDescricao_Colecao: TDBEdit;
    DBCODIGO_MODELISTA: TDBEdit;
    DBNome_Modelista: TDBEdit;
    panelGrade: TPanel;
    Label2: TLabel;
    SpeedButton2: TSpeedButton;
    Label3: TLabel;
    SpeedButton3: TSpeedButton;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Q_Busca_Numeros_Grade: TOracleDataSet;
    Q_Grade: TOracleDataSet;
    Q_GradeDESCRICAO: TStringField;
    Q_GradeID_TIPO_SOLADO: TFloatField;
    Q_Busca_Numeros_GradeNUMERACAO1: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO2: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO3: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO4: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO5: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO6: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO7: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO8: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO9: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO10: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO11: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO12: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO13: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO14: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO15: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO16: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO17: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO18: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO19: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO20: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO21: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO22: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO23: TStringField;
    Q_Busca_Numeros_GradeNUMERACAO24: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO1: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO2: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO3: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO4: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO5: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO6: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO7: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO8: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO9: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO10: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO11: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO12: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO13: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO14: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO15: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO16: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO17: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO18: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO19: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO20: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO21: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO22: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO23: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO24: TStringField;
    panelTitulo: TPanel;
    pn1: TPanel;
    DBT1: TDBText;
    DBCheckBox2: TDBCheckBox;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO1: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO2: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO3: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO4: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO5: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO6: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO7: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO8: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO9: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO10: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO11: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO12: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO13: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO14: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO15: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO16: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO17: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO18: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO19: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO20: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO21: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO22: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO23: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO24: TStringField;
    pn25: TPanel;
    DBT25: TDBText;
    DBCheckBox4: TDBCheckBox;
    pn23: TPanel;
    DBT23: TDBText;
    DBCheckBox5: TDBCheckBox;
    pn22: TPanel;
    DBT22: TDBText;
    DBCheckBox6: TDBCheckBox;
    pn21: TPanel;
    DBT21: TDBText;
    DBCheckBox7: TDBCheckBox;
    pn20: TPanel;
    DBT20: TDBText;
    DBCheckBox8: TDBCheckBox;
    pn19: TPanel;
    DBT19: TDBText;
    DBCheckBox9: TDBCheckBox;
    pn18: TPanel;
    DBT18: TDBText;
    DBCheckBox10: TDBCheckBox;
    pn17: TPanel;
    DBT17: TDBText;
    DBCheckBox11: TDBCheckBox;
    pn16: TPanel;
    DBT16: TDBText;
    DBCheckBox12: TDBCheckBox;
    pn15: TPanel;
    DBT15: TDBText;
    DBCheckBox13: TDBCheckBox;
    pn14: TPanel;
    DBT14: TDBText;
    DBCheckBox14: TDBCheckBox;
    pn13: TPanel;
    DBT13: TDBText;
    DBCheckBox15: TDBCheckBox;
    pn12: TPanel;
    DBT12: TDBText;
    DBCheckBox16: TDBCheckBox;
    pn11: TPanel;
    DBT11: TDBText;
    DBCheckBox17: TDBCheckBox;
    pn10: TPanel;
    DBT10: TDBText;
    DBCheckBox18: TDBCheckBox;
    pn9: TPanel;
    DBT9: TDBText;
    DBCheckBox19: TDBCheckBox;
    pn8: TPanel;
    DBT8: TDBText;
    DBCheckBox20: TDBCheckBox;
    pn7: TPanel;
    DBT7: TDBText;
    DBCheckBox21: TDBCheckBox;
    pn6: TPanel;
    DBT6: TDBText;
    DBCheckBox22: TDBCheckBox;
    pn5: TPanel;
    DBT5: TDBText;
    DBCheckBox23: TDBCheckBox;
    pn4: TPanel;
    DBT4: TDBText;
    DBCheckBox24: TDBCheckBox;
    DBT3: TDBText;
    DBCheckBox25: TDBCheckBox;
    pn2: TPanel;
    DBT2: TDBText;
    DBCheckBox26: TDBCheckBox;
    pn3: TPanel;
    Q_Busca_Numeros_GradeNUMERACAO25: TStringField;
    Q_Busca_Numeros_GradeSTATUSNUMERACAO25: TStringField;
    Q_Busca_Numeros_GradeVISIVELNUMERACAO25: TStringField;
    pn24: TPanel;
    DBT24: TDBText;
    DBCheckBox1: TDBCheckBox;
    Panel1: TPanel;
    Label4: TLabel;
    SpeedButton4: TSpeedButton;
    Label5: TLabel;
    SpeedButton5: TSpeedButton;
    DBEdit2: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    btn_Ok: TBitBtn;
    Shape1: TShape;
    Label1: TLabel;
    procedure DBGRADEDblClick(Sender: TObject);
    procedure DBGRADEKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGRADEExit(Sender: TObject);
    procedure btn_OkClick(Sender: TObject);

    procedure DBT1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
   

    
  private
    { Private declarations }
     procedure preencheGrade();
     procedure marcarNumeroBase(componente :TObject);
  public

  end;

var
  Frm_Vizualizar_Grade: TFrm_Vizualizar_Grade;

implementation

{$R *.dfm}

uses UCad_Referencia, Unit_funcoes, UCons_Grade, UDM;

procedure TFrm_Vizualizar_Grade.btn_OkClick(Sender: TObject);
var i : integer;
begin

  //PERCORRE OS NUMEROS PARA ACHAR O NUMERO BASE MARCADO
   for i:= 1 to 25 do
   begin
     if TDBText( FindComponent( 'DBT'+inttostr(i))).Font.Color = clWebSaddleBrown then
     begin
       dm.Q_ReferenciaNUMERO_BASE.AsInteger := StrToInt(TDBText( FindComponent( 'DBT'+inttostr(i))).Caption);
     end;
   end;

  Frm_Vizualizar_Grade.close;
end;

procedure TFrm_Vizualizar_Grade.DBGRADEDblClick(Sender: TObject);
begin

    Frm_Cons_Grade := TFrm_Cons_Grade.create(self);
    Frm_Cons_Grade.P_Sessao            := DM.OraBanco;
    Frm_Cons_Grade.p_user              := FrmPrincipal.conexao.User;
    Frm_Cons_Grade.p_password          := FrmPrincipal.conexao.Password;
    Frm_Cons_Grade.m_diretorio_sistema := FrmPrincipal.conexao.Diretorio_sistema;

    if Frm_Cons_Grade.ShowModal = mrOk then
      DM.Q_ReferenciaID_GRADE.Value := Frm_Cons_Grade.Q_Cons_GradeID_GRADE.Value;
end;

procedure TFrm_Vizualizar_Grade.DBGRADEExit(Sender: TObject);

begin
  //busca a grade para montar
  if DBGRADE.Text <> '' then
    begin
      preencheGrade();
    end;

end;

procedure TFrm_Vizualizar_Grade.DBGRADEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = vk_f8 then
      (sender as TDBEdit).OnDblClick(self);
end;

procedure TFrm_Vizualizar_Grade.DBT1DblClick(Sender: TObject);
begin
   marcarNumeroBase(Sender);
end;

procedure TFrm_Vizualizar_Grade.FormCreate(Sender: TObject);
var i :integer;
begin
   if DM.Q_Referencia.State in dsEditModes then
    begin
      preencheGrade();
    end;
end;

procedure TFrm_Vizualizar_Grade.FormShow(Sender: TObject);
var i:integer;
begin
  DBGRADEExit(sender);

  //PINTA NUMERO BASE
  for i:= 1 to 25 do
  begin
    if dm.Q_ReferenciaNUMERO_BASE.AsString = TDBText( FindComponent( 'DBT'+inttostr(i))).Caption then
    begin
      TPanel( FindComponent( 'pn'+inttostr(i))).Color := clWebOrange;
      TDBText( FindComponent( 'DBT'+inttostr(i))).Font.Color := clWebSaddleBrown;
    end;
  end;
end;

procedure TFrm_Vizualizar_Grade.marcarNumeroBase(componente :TObject);
var i:integer;
    nomeComponente , nomePanel, cor: String;
begin
 if (componente.ClassName = 'TDBText') then
   begin
     nomeComponente := TDBText(componente).Name;
     nomePanel := 'pn'+nomeComponente.Substring(3, nomeComponente.Length);

     if  TDBText( FindComponent( nomeComponente)).Font.Color = clWebSaddleBrown then
       begin
         TPanel( FindComponent(nomePanel) ).color := clWhite;
         TDBText( FindComponent( nomeComponente)).Font.Color := clNavy;
       end
     else
       begin
          for i:= 1 to 25 do
            begin
              TPanel( FindComponent( 'pn'+inttostr(i))).Color := clWhite;
              TDBText( FindComponent( 'DBT'+inttostr(i))).Font.Color := clNavy;
            end;
          TPanel( FindComponent(nomePanel) ).color := clWebOrange;
          TDBText( FindComponent( nomeComponente)).Font.Color := clWebSaddleBrown;
       end;
    end;
end;


procedure TFrm_Vizualizar_Grade.preencheGrade();
var countNumeracao, countStatus, countVisivel, i : integer;
  teste : string;
  t : TStringField;
begin

    Q_Busca_Numeros_Grade.Close;
    Q_Busca_Numeros_Grade.SetVariable('p_id_grade', DM.Q_ReferenciaID_GRADE.AsString);
    Q_Busca_Numeros_Grade.Open;

    //habilitando os panels
    for i:= 1 to 25 do
      begin

        teste :=  TStringField( FindComponent( 'Q_Busca_Numeros_GradeVISIVELNUMERACAO' + inttostr(i) )).AsString;

        if teste = 'S' then
           TPanel( FindComponent( 'pn'+inttostr(i))).Visible := true
        else
           TPanel( FindComponent( 'pn'+inttostr(i))).Visible := false;
     end;

    
    if DM.Q_Referencia.State = dsedit then
      begin
        with FrmPrincipal do
          begin                

            DM.Q_Referencia_Produto_Numero.Close;
            DM.Q_Referencia_Produto_Numero.SetVariable('p_codigo_produto', DM.Q_ReferenciaID_REFERENCIA.AsString);
            DM.Q_Referencia_Produto_Numero.Open;

            DM.MemoryTable.Close;
            DM.MemoryTable.Open;

            //PEGANDO A DESCRICAO DO NUMERO E JOGANDO PARA O MEMORY
            DM.MemoryTable.Append;

            DM.Q_Referencia_Produto_Numero.First;
            while not DM.Q_Referencia_Produto_Numero.Eof do
              begin
                TStringField( DM.FindComponent( 'MemoryTableNUMERACAO' +  DM.Q_Referencia_Produto_NumeroPOSICAO.AsString)).AsString :=
                DM.Q_Referencia_Produto_NumeroNUMERO.AsString;

                TStringField( DM.FindComponent( 'MemoryTableSTATUSNUMERACAO' +  DM.Q_Referencia_Produto_NumeroPOSICAO.AsString)).AsString :=
                DM.Q_Referencia_Produto_NumeroATIVO.AsString;
                DM.Q_Referencia_Produto_Numero.Next;
              end;
            DM.MemoryTable.Post;
          end; 
      end
    else
      begin         

        //PEGANDO A DESCRICAO DO NUMERO E JOGANDO PARA O MEMORY
        DM.MemoryTable.Append;
        for i:= 1 to 25 do
          begin

            TStringField( DM.FindComponent( 'MemoryTableNUMERACAO' + inttostr(i) )).AsString :=
                TStringField( FindComponent( 'Q_Busca_Numeros_GradeNUMERACAO' + inttostr(i) )).AsString;

            TStringField( DM.FindComponent( 'MemoryTableSTATUSNUMERACAO' + inttostr(i) )).AsString :=
                 TStringField( FindComponent( 'Q_Busca_Numeros_GradeSTATUSNUMERACAO' + inttostr(i) )).AsString;

          end;

        DM.MemoryTable.Post;
        
      end;
      
  end;

end.
