@echo off
dir

del *.dof
del *.cfg
del *.exe
del *.dcu
del *.local
del *.identcache
del *.~*

attrib -r -a -s -h /s /d

del __history

exit;