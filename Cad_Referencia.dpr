program Cad_Referencia;

uses
  Forms,
  UCad_Referencia in 'UCad_Referencia.pas' {FrmPrincipal},
  Unit_funcoes in '..\Classes\Unit_funcoes.pas',
  UImpressao in '..\Classes\UImpressao.pas' {FRM_Impressao},
  UVizualizarImagem in '..\Classes\UVizualizarImagem.pas' {Frm_Vizualizar_Imagem},
  UDuplicar_Referencia in 'UDuplicar_Referencia.pas' {Frm_Duplicar_Referencia},
  UCons_Referencia in '..\Unit_Consulta\UCons_Referencia.pas' {Frm_Cons_Referencia},
  UCons_Tipo_Produto in '..\Unit_Consulta\UCons_Tipo_Produto.pas' {Frm_Cons_Tipo_Produto},
  UCons_Cor in '..\Unit_Consulta\UCons_Cor.pas' {Frm_Cons_Cor},
  UCons_Nivel_Conteudo in '..\Unit_Consulta\UCons_Nivel_Conteudo.pas' {Frm_Cons_Nivel_Conteudo},
  UCons_Forma in '..\Unit_Consulta\UCons_Forma.pas' {Frm_Cons_Forma},
  UCons_Tipo_Material in '..\Unit_Consulta\UCons_Tipo_Material.pas' {Frm_Cons_Tipo_Material},
  UCons_Tipo_Solado in '..\Unit_Consulta\UCons_Tipo_Solado.pas' {Frm_Cons_Tipo_Solado},
  UCons_Tipo_Corte in '..\Unit_Consulta\UCons_Tipo_Corte.pas' {Frm_Cons_Tipo_Corte},
  UCons_Colecao in '..\Unit_Consulta\UCons_Colecao.pas' {Frm_Cons_Colecao},
  UCons_Evento in '..\Unit_Consulta\UCons_Evento.pas' {Frm_Cons_Evento},
  UCons_Designer in '..\Unit_Consulta\UCons_Designer.pas' {Frm_Cons_Designer},
  UCons_Modelista in '..\Unit_Consulta\UCons_Modelista.pas' {Frm_Cons_Modelista},
  UCons_Empresa in '..\Unit_Consulta\UCons_Empresa.pas' {Frm_Cons_Empresa},
  UCons_Grade in '..\Unit_Consulta\UCons_Grade.pas' {Frm_Cons_Grade},
  UVisualizar_Grade in 'UVisualizar_Grade.pas' {Frm_Vizualizar_Grade},
  UDM in 'UDM.pas' {DM: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TFrm_Cons_Tipo_Corte, Frm_Cons_Tipo_Corte);
  Application.CreateForm(TFrm_Cons_Colecao, Frm_Cons_Colecao);
  Application.CreateForm(TFrm_Cons_Evento, Frm_Cons_Evento);
  Application.CreateForm(TFrm_Cons_Designer, Frm_Cons_Designer);
  Application.CreateForm(TFrm_Cons_Modelista, Frm_Cons_Modelista);
  Application.CreateForm(TFrm_Cons_Empresa, Frm_Cons_Empresa);
  Application.CreateForm(TFrm_Cons_Grade, Frm_Cons_Grade);
  Application.CreateForm(TFrm_Vizualizar_Grade, Frm_Vizualizar_Grade);

  Application.Run;
end.
